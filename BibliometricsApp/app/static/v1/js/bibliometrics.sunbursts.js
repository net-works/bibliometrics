var chart;
function sunburst(data)
{
	nv.addGraph(function() {
		chart = nv.models.sunburstChart();
		chart.color(d3.scale.category20c());

		d3.select("#sunburst")
		  .datum(data)
		  .call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}