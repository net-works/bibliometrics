var help_height = Math.max($("#help-scope .help-panel").height(), $("#help-tab .help-panel").height(), $("#help-viz .help-panel").height())
$("#help-scope .help-panel").height(help_height)
$("#help-tab .help-panel").height(help_height)
$("#help-viz .help-panel").height(help_height)

$("#help-btn").on('click', function(){
  if( $("#help-btn").hasClass("active") ){
    $("#help-scope").hide("blind",300);
    $("#help-tab").hide("blind",300);
    $("#help-viz").hide("blind",300);
    $("#help-btn").removeClass("active")
                  .removeClass("red lighten-2")
                  .addClass("scielo-blue lighten-1");
    $("#help-icon").show();
    $("#help-icon-cancel").hide();
  }else{
    $("#help-scope").show("blind",300);  
    $("#help-tab").delay(300).show("blind",300);
    $("#help-viz").delay(600).show("blind",300);
    $("#help-btn").addClass("active")
                  .removeClass("scielo-blue lighten-1")
                  .addClass("red lighten-2");
    $("#help-icon").hide();
    $("#help-icon-cancel").show();
  }
});