function export_svg_as_slide(svg_element, data){
  //
  var img = data;
  var width = 7.5;    
  var factor = width/$(svg_element).width();
  var height = $(svg_element).height() * factor;
  // Create a new Presentation
  var pptx = new PptxGenJS();

  // Add a Slide, then add objects
  var slide = pptx.addNewSlide();
  slide.addText('Scielo Analytics Visualizations', { x:0.5, y:0.25, font_size:18, font_face:'Arial', color:'0088CC' });
  slide.addImage({ data: img, x: 1.0, y: 1.0, w: width, h: height });

  //Filtros
  var slide = pptx.addNewSlide();
  slide.addText('Filters', { x:0.5, y:0.25, font_size:18, font_face:'Arial', color:'0088CC'});
  slide.addText('Scope: '+JSON_CURRENT_FILTERS['scope'], { x:0.5, y:0.60, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('Tabulation: '+JSON_CURRENT_FILTERS['tabulation'], { x:0.5, y:0.90, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('Visualization: '+JSON_CURRENT_FILTERS['visualization'], { x:0.5, y:1.2, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText(ppt_show_filters_with_operator1('collections', 'collections_operator', 'Collection'), { x:0.5, y:1.5, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('Journal Status: '+JSON_CURRENT_FILTERS['journals_status'], { x:0.5, y:1.8, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText(ppt_show_filters_with_operator2('journals', 'journals_operator', 'Journal'), { x:0.5, y:2.1, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText(ppt_show_filters_with_operator1('languages', 'languages_operator', 'Language'), { x:0.5, y:2.4, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText(ppt_show_filters_with_operator2('thematic_areas', 'thematic_areas_operator', 'Thematic Area'), { x:0.5, y:2.7, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText(ppt_show_filters_with_operator2('wos_thematic_areas', 'wos_thematic_areas_operator', 'WOS Thematic Area'), { x:0.5, y:3.0, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('Document Type: '+JSON_CURRENT_FILTERS['document_type'], { x:0.5, y:3.3, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText(ppt_show_filters_with_operator2('document_type_details', 'document_type_details_operator', 'Document Type Detail'), { x:0.5, y:3.6, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('Year: '+JSON_CURRENT_FILTERS['years'], { x:0.5, y:3.9, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('From Year: '+JSON_CURRENT_FILTERS['from_year'], { x:0.5, y:4.2, font_size:12, font_face:'Arial', color:'0088CC'});
  slide.addText('To Year: '+JSON_CURRENT_FILTERS['to_year'], { x:0.5, y:4.5, font_size:12, font_face:'Arial', color:'0088CC'});


  // Export Presentation
  pptx.save('[SciELO]Visualization');
}

function ppt_show_filters_with_operator1(key, operator_key, name){
  filters_string = ""
  if(JSON_CURRENT_FILTERS.hasOwnProperty(key)){
    filters_string += name + ": "
    for(c in JSON_CURRENT_FILTERS[key]){
      if(c < JSON_CURRENT_FILTERS[key].length-1){
        if(JSON_CURRENT_FILTERS.hasOwnProperty(operator_key)){
          filters_string += JSON_CURRENT_FILTERS[key][c][1] + " and ";
        }else{
          filters_string += JSON_CURRENT_FILTERS[key][c][1] + " or ";
        }
      }else{
        filters_string += JSON_CURRENT_FILTERS[key][c][1]
      }
    }
  }
  return filters_string
}
function ppt_show_filters_with_operator2(key, operator_key, name){
  filters_string = ""
  if(JSON_CURRENT_FILTERS.hasOwnProperty(key)){
    filters_string += name + ": "
    for(c in JSON_CURRENT_FILTERS[key]){
      if(c < JSON_CURRENT_FILTERS[key].length-1){
        if(JSON_CURRENT_FILTERS.hasOwnProperty(operator_key)){
          filters_string += JSON_CURRENT_FILTERS[key][c] + " and ";
        }else{
          filters_string += JSON_CURRENT_FILTERS[key][c] + " or ";
        }
      }else{
        filters_string += JSON_CURRENT_FILTERS[key][c]
      }
    }
  }
  return filters_string
}


function makePDF(svg_element, data){
  var pdf = new jsPDF("p", "mm", "a4");
  var width = pdf.internal.pageSize.width - 20;    
  ($(svg_element).width() > width) ? factor = width/$(svg_element).width() : factor = 1.0;
  var height = $(svg_element).height() * factor;
  pdf.addImage(data, 'png', 10, 10, width, height);
  pdf.save('[SciELO]Visualization.pdf');
}

function export_visualization(svg_element, extension){
  var svgData = $(svg_element)[0].outerHTML;
  var dataType = "image/svg+xml;charset=utf-8";
  var svgBlob = new Blob([svgData], {type: dataType});
  
  if(extension == 'svg'){
    var url = URL.createObjectURL(svgBlob);
    download_visualization(url, extension);
  }else if(['png', 'jpeg', 'pptx', 'pdf'].indexOf(extension) >= 0){
    var canvas = document.getElementById("exporter-canvas");
    canvas.width = $(svg_element).width();
    canvas.height = $(svg_element).height();
    //Problemas de resolución
    var pixelRatio = window.devicePixelRatio || 1;
    canvas.width *= pixelRatio;
    canvas.height *= pixelRatio;
    //--
    var ctx = canvas.getContext("2d");
    var DOMURL = self.URL || self.webkitURL || self;
    var img = new Image();
    var url = DOMURL.createObjectURL(svgBlob);
    img.src = url;
    img.onload = function() {
      ctx.drawImage(img, 0, 0);
      if(['png', 'jpeg'].indexOf(extension) >= 0){
        var base64_image = canvas.toDataURL("image/"+extension, 1.0);
        download_visualization(base64_image, extension);
      }else if(extension == 'pptx'){
        var base64_image = canvas.toDataURL("image/png", 1.0);
        export_svg_as_slide(svg_element, base64_image);
      }else if(extension == 'pdf'){
        var base64_image = canvas.toDataURL("image/png", 1.0);
        makePDF(svg_element, base64_image);
      }
      DOMURL.revokeObjectURL(base64_image);
    };
  } 
}

function download_visualization(image, extension){
  var downloadLink = document.createElement("a");
  downloadLink.href = image;
  downloadLink.download = "[Scielo]Visualization."+extension;
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}