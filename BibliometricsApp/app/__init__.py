#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Import flask and template operators
from flask import Flask, Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash
from flask_compress import Compress
from flask_babel import Babel, refresh

# Define the WSGI application object
app = Flask(__name__)
Compress(app)
# Configurations
app.config.from_object('config')
#Babel
babel = Babel(app)

#VERSION 3
#-------------------------------------------------------------------------------------------
# Import a module / component using its blueprint handler variable (mod_xyz)
from app.v1.controllers.indexController import mod_index as index_v1_module
app.register_blueprint(index_v1_module)
from app.v1.controllers.documentController import mod_document as document_v1_module
app.register_blueprint(document_v1_module)
from app.v1.controllers.journalController import mod_journal as journal_v1_module
app.register_blueprint(journal_v1_module)
from app.v1.controllers.relationshipController import mod_relationship as relationship_v1_module
app.register_blueprint(relationship_v1_module)
from app.v1.controllers.exporterController import mod_export as export_v1_module
app.register_blueprint(export_v1_module)
#-------------------------------------------------------------------------------------------

# Error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500
@app.errorhandler(502)
def internal_error(error):
    return render_template('502.html'), 502


#Babel local selector
LANGUAGES = {
    'en': 'English',
    'es': 'Español',
    'pt': "Português"
}

@babel.localeselector
def get_locale():
    if 'lang' not in session:
        session['lang'] = request.accept_languages.best_match(list(LANGUAGES.keys()))
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    elif request.form.get('lang'):
        session['lang'] = request.form.get('lang')
    if session['lang'] is None:
        session['lang'] = 'en'
    return session.get('lang')