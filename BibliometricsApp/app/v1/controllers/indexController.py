#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.helper_dicts.COLLECTION_DICTS import COUNTRY_COLLECTIONS,\
                                              NOT_COLLECTIONS,\
                                              COLLECTIONS
from app.helper_dicts.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.helper_dicts.ISO_639_ALPHA2 import ISO_639_ALPHA2
from app.v1.helpers.elasticQueries import elasticQueries
from app.v1.classes.filters import Filters
from app.v1.classes.publicationStats import PublicationStats

import pyexcel as pe
import io
import requests
import os.path
import random
import json

VERSION = "v1"
mod_index = Blueprint('v1_indexController', __name__, url_prefix='/'+VERSION)

#-- ----------------------------------------------------------------

@mod_index.route('/')
def index():
    filters = Filters()
    if request.args.get('lang'):
        filters.lang = request.args.get('lang')
    elif request.form.get('lang'):
        filters.lang = request.form.get('lang')
        
    cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/data/index.json'
    if not os.path.exists(cache_filename):
        publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
        response = publication.data_index()
        by_collection = response["aggregations"]["by_collection"]["buckets"]
        data = []
        for i in by_collection:
          if str(i["key"]) in COUNTRY_COLLECTIONS:       
            alpha3 = COUNTRY_COLLECTIONS[str(i["key"])]
            data.append({
              "country-code": ISO_3166_ALPHA3[alpha3]["code"],
              "country-name": ISO_3166_ALPHA3[alpha3]["fullname"],
              "documents": i["doc_count"],
              "journals": len(i["by_journal"]["buckets"])
            })
        j = { 'data': data }
        with open(cache_filename, 'w') as json_file:
            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
    with open(cache_filename) as data_file:    
        json_data = json.load(data_file)
        data = json_data['data']

    return render_template("v1/index/index.html", filters = filters, data = data).encode( "utf-8" )

#-- ----------------------------------------------------------------

@mod_index.route('/history')
def history():
    filters = Filters()
    return render_template("v1/index/history.html", filters = filters, ISO_3166_ALPHA3 = ISO_3166_ALPHA3,
                            country_collections = COUNTRY_COLLECTIONS, not_collections = NOT_COLLECTIONS,
                            ISO_639_ALPHA2 = ISO_639_ALPHA2)

#-- ----------------------------------------------------------------

@mod_index.route('/user_history')
def user_history():
    filters = Filters()
    return render_template("v1/index/user_history.html", filters = filters, ISO_3166_ALPHA3 = ISO_3166_ALPHA3,
                            country_collections = COUNTRY_COLLECTIONS, not_collections = NOT_COLLECTIONS,
                            ISO_639_ALPHA2 = ISO_639_ALPHA2)

#-- ----------------------------------------------------------------
#Conjunto de métodos para crear el json de filtros avanzados
#-- ----------------------------------------------------------------
@mod_index.route('/gfilters')
def gfilters():
    #Obtenemos información para los filtros
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    data = {}
    data['thematicAreas']  = get_subject_areas(publication)
    data['wosThematicAreas']  = get_wos_subject_areas(publication)
    data['collections'] = [ [ c, COLLECTIONS[c][0] ] for c in get_collections(publication) ]
    data['documentTypes'] = get_document_types(publication)
    
    #Languages
    data['languages'] = []
    languages = get_languages(publication)
    for l in languages:
        data['languages'].append([l, ISO_639_ALPHA2[l]['english']] if l in ISO_639_ALPHA2 else [l, l]) 
    
    data['journals'] = get_journals(publication)
    return jsonify(data)

#-- ----------------------------------------------------------------

def get_thematic_areas(publication):
    response = publication.query_3()
    return [ a['key'] for a in response["aggregations"]["by_area"]["buckets"] ]

def get_subject_areas(publication):
    response = publication.query_article_distinct_field("subject_areas")
    return [ t['key'] for t in response["aggregations"]["by_value"]["buckets"] ]

def get_wos_subject_areas(publication):
    response = publication.query_article_distinct_field("wos_subject_areas")
    return [ t['key'] for t in response["aggregations"]["by_value"]["buckets"] ]

def get_collections(publication):
    response = publication.query_article_distinct_field("collection")
    return [ c['key'] for c in response["aggregations"]["by_value"]["buckets"] ]

def get_document_types(publication):
    response = publication.query_article_distinct_field("document_type")
    return [ dt['key'] for dt in response["aggregations"]["by_value"]["buckets"] ]

def get_languages(publication):
    response = publication.query_article_distinct_field("languages")
    return [ l['key'] for l in response["aggregations"]["by_value"]["buckets"] ]

def get_journals(publication):
    response = publication.query_article_distinct_field("journal_title")
    return [ j['key'] for j in response["aggregations"]["by_value"]["buckets"] ]

#-- ----------------------------------------------------------------
#Fin métodos para crear el json de filtros avanzados
#-- ----------------------------------------------------------------