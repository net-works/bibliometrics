#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  current_app
from flask_babel import gettext, ngettext

import pyexcel as pe
import io
import time
import ast

VERSION = "v1"
mod_export = Blueprint('v1_exporterController', __name__, url_prefix='/'+VERSION)


def gfile_download(filename = 'export', extension = 'xlsx', book = {}):
    #generamos el excel
    book = pe.Book(book)
    io1 = io.BytesIO()
    book.save_to_memory(extension, io1)
    output = make_response(io1.getvalue())
    output.headers["Content-Disposition"] = "attachment; filename=%s.%s" %(filename, extension)
    output.headers["Content-type"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    return output


@mod_export.route('/export', methods = ['POST'])
def export():
    #Leemos Formulario y obtenemos filtros
    form =  request.form
    
    header1 = form.get('header1', 'Header1')
    header2 = form.get('header2', 'Header2')
    filters = ast.literal_eval(form.get('filters', '{}'))
    data = ast.literal_eval(form.get('data', '{}'))

    return simple_document_dict_export(data, filters, header1, header2)
    #return gfile_download("[SciELO]Empty", "xlsx", book)

@mod_export.route('/export_journal', methods = ['POST'])
def export_journal():
    #Leemos Formulario y obtenemos filtros
    form =  request.form
    
    header1 = form.get('header1', 'Header1')
    header2 = form.get('header2', 'Header2')
    filters = ast.literal_eval(form.get('filters', '{}'))
    data = ast.literal_eval(form.get('data', '{}'))

    return simple_journal_dict_export(data, filters, header1, header2)

def simple_journal_dict_export(data = {}, filters = {}, header1 = 'Header1', header2 = 'Header2'):
    book = {}
    dt = time.gmtime()
    data_sheet = "Data"
    book[data_sheet] = []
    book[data_sheet].append([gettext("Date"),
                             gettext("Time"),
                             gettext(header1),
                             gettext(header2),
                             gettext("# Documents"),
                             gettext("# Journals")
                            ])
    for key, values in list(data.items()):
        for k, v in list(values.items()):
            book[data_sheet].append([time.strftime("%d-%m-%Y", dt),
                                     time.strftime("%H:%M:%S", dt),
                                     key,
                                     k,
                                     v['documents'],
                                     v['journals']
                                    ])
    #Filters
    filters_sheet = "Filters"
    book[filters_sheet] = []
    if 'scope' in filters:
        book[filters_sheet].append(['Scope', filters['scope']])
    if 'tabulation' in filters:
        book[filters_sheet].append(['Tabulation', filters['tabulation']])
    if 'collections' in filters:
        for c in filters['collections']:
            book[filters_sheet].append(['Collection', c[1]])
    if 'journals_status' in filters:
        book[filters_sheet].append(['Journal Status', filters['journals_status']])
    if 'journals' in filters:
        for j in filters['journals']:
            book[filters_sheet].append(['Journal', j])
    if 'languages' in filters:
        for l in filters['languages']:
            book[filters_sheet].append(['Language', l[1]])
    if 'thematic_areas' in filters:
        for ta in filters['thematic_areas']:
            book[filters_sheet].append(['SciELO Thematic Area', ta])
    if 'wos_thematic_areas' in filters:
        for wta in filters['wos_thematic_areas']:
            book[filters_sheet].append(['WOS Thematic Area', wta])
    if 'document_type' in filters:
        book[filters_sheet].append(['Document Type', filters['document_type']])
    if 'document_type_details' in filters:
        for dtd in filters['document_type_details']:
            book[filters_sheet].append(['Document', dtd])
    return gfile_download("[SciELO]Journals", "xlsx", book)

def simple_document_dict_export(data = {}, filters = {}, header1 = 'Header1', header2 = 'Header2'):
    book = {}
    dt = time.gmtime()
    data_sheet = "Data"
    book[data_sheet] = []
    book[data_sheet].append(["","","","","",gettext("Documents by Language"),"","","","",gettext("Documents by Thematic Area"),"","","","","","","","","",gettext("Citable Documents Type"),"","","","","","","","","",""])
    book[data_sheet].append([gettext("Date"),
                             gettext("Time"),
                             gettext(header1),
                             gettext(header2),
                             gettext("# Documents"),
                             gettext("English"),
                             gettext("Spanish"),
                             gettext("Portuguese"),
                             gettext("Other"),
                             gettext("Total"),
                             gettext("Health Sciences"),
                             gettext("Human Sciences"),
                             gettext("Biological Sciences"),
                             gettext("Agricultural Sciences"),
                             gettext("Applied Social Sciences"),
                             gettext("Exact and Earth Sciences"),
                             gettext("Engineering"),
                             gettext("Linguistics, Letters and Arts"),
                             gettext("Undefined"),
                             gettext("Total"),
                             gettext("Research Article"),
                             gettext("Editorial"),
                             gettext("Book Review"),
                             gettext("Case Report"),
                             gettext("Review Article"),
                             gettext("Rapid Communication"),
                             gettext("Letter"),
                             gettext("Brief Report"),
                             gettext("Article Commentary"),
                             gettext("Abstract"),
                             gettext("Total")
                            ])
    for key, values in list(data.items()):
        for k, v in list(values.items()):
            book[data_sheet].append([time.strftime("%d-%m-%Y", dt),
                                     time.strftime("%H:%M:%S", dt),
                                     key,
                                     k,
                                     v['documents'],
                                     v['by_language']['en'],
                                     v['by_language']['es'],
                                     v['by_language']['pt'],
                                     v['by_language']['other'],
                                     v['by_language']['total'],
                                     v['by_thematic_area']["Health Sciences"],
                                     v['by_thematic_area']["Human Sciences"],
                                     v['by_thematic_area']["Biological Sciences"],
                                     v['by_thematic_area']["Agricultural Sciences"],
                                     v['by_thematic_area']["Applied Social Sciences"],
                                     v['by_thematic_area']["Exact and Earth Sciences"],
                                     v['by_thematic_area']["Engineering"],
                                     v['by_thematic_area']["Linguistics, Letters and Arts"],
                                     v['by_thematic_area']["undefined"],
                                     v['by_thematic_area']["total"],
                                     v['by_citable_doc']["research-article"],
                                     v['by_citable_doc']["editorial"],
                                     v['by_citable_doc']["book-review"],
                                     v['by_citable_doc']["case-report"],
                                     v['by_citable_doc']["review-article"],
                                     v['by_citable_doc']["rapid-communication"],
                                     v['by_citable_doc']["letter"],
                                     v['by_citable_doc']["brief-report"],
                                     v['by_citable_doc']["article-commentary"],
                                     v['by_citable_doc']["abstract"],
                                     v['by_citable_doc']["total"]
                                    ])
    #Filters
    filters_sheet = "Filters"
    book[filters_sheet] = []
    if 'scope' in filters:
        book[filters_sheet].append(['Scope', filters['scope']])
    if 'tabulation' in filters:
        book[filters_sheet].append(['Tabulation', filters['tabulation']])
    if 'collections' in filters:
        for c in filters['collections']:
            book[filters_sheet].append(['Collection', c[1]])
    if 'journals_status' in filters:
        book[filters_sheet].append(['Journal Status', filters['journals_status']])
    if 'journals' in filters:
        for j in filters['journals']:
            book[filters_sheet].append(['Journal', j])
    if 'languages' in filters:
        for l in filters['languages']:
            book[filters_sheet].append(['Language', l[1]])
    if 'thematic_areas' in filters:
        for ta in filters['thematic_areas']:
            book[filters_sheet].append(['SciELO Thematic Area', ta])
    if 'wos_thematic_areas' in filters:
        for wta in filters['wos_thematic_areas']:
            book[filters_sheet].append(['WOS Thematic Area', wta])
    if 'document_type' in filters:
        book[filters_sheet].append(['Document Type', filters['document_type']])
    if 'document_type_details' in filters:
        for dtd in filters['document_type_details']:
            book[filters_sheet].append(['Document', dtd])
    return gfile_download("[SciELO]Documents", "xlsx", book)