#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA3 = {
	"DZA": {
		"alpha2": "DZ",
		"code": "afdza",
		"pt": "Algeria",
		"en": "Algeria",
		"fullname": "Algeria",
		"continent": "af",
		"es": "Algeria"
	},
	"AGO": {
		"alpha2": "AO",
		"code": "afago",
		"pt": "Angola",
		"en": "Angola",
		"fullname": "Angola",
		"continent": "af",
		"es": "Angola"
	},
	"BGD": {
		"alpha2": "BD",
		"code": "asbgd",
		"pt": "Bangladesh",
		"en": "Bangladesh",
		"fullname": "Bangladesh",
		"continent": "as",
		"es": "Bangladesh"
	},
	"QAT": {
		"alpha2": "QA",
		"code": "asqat",
		"pt": "Catar",
		"en": "Qatar",
		"fullname": "Qatar",
		"continent": "as",
		"es": "Qatar"
	},
	"BGR": {
		"alpha2": "BG",
		"code": "eubgr",
		"pt": "Bulgária",
		"en": "Bulgaria",
		"fullname": "Bulgaria",
		"continent": "eu",
		"es": "Bulgaria"
	},
	"PAK": {
		"alpha2": "PK",
		"code": "aspak",
		"pt": "Paquistão",
		"en": "Pakistan",
		"fullname": "Pakistan",
		"continent": "as",
		"es": "Pakistán"
	},
	"CPV": {
		"alpha2": "CV",
		"code": "afcpv",
		"pt": "Cabo Verde",
		"en": "Cape Verde",
		"fullname": "Cape Verde Islands",
		"continent": "af",
		"es": "Cabo Verde"
	},
	"VNM": {
		"alpha2": "VN",
		"code": "asvnm",
		"pt": "Vietnã",
		"en": "Vietnam",
		"fullname": "Vietnam",
		"continent": "as",
		"es": "Vietnam"
	},
	"TZA": {
		"alpha2": "TZ",
		"code": "aftza",
		"pt": "Tanzânia",
		"en": "Tanzania",
		"fullname": "Zanzibar",
		"continent": "af",
		"es": "Tanzania"
	},
	"BWA": {
		"alpha2": "BW",
		"code": "afbwa",
		"pt": "Botsuana",
		"en": "Botswana",
		"fullname": "Botswana",
		"continent": "af",
		"es": "Botsuana"
	},
	"SPM": {
		"alpha2": "PM",
		"code": "naspm",
		"pt": "San Pedro y Miquelón",
		"en": "Saint Pierre and Miquelon",
		"fullname": "St. Pierre and Miquelon",
		"continent": "na",
		"es": "San Pedro y Miquelón"
	},
	"KNA": {
		"alpha2": "KN",
		"code": "nakna",
		"pt": "San Cristóbal y Nieves",
		"en": "Saint Kitts and Nevis",
		"fullname": "St. Kitts",
		"continent": "na",
		"es": "San Cristóbal y Nieves"
	},
	"GIB": {
		"alpha2": "GI",
		"code": "eugib",
		"pt": "Gibraltar",
		"en": "Gibraltar",
		"fullname": "Gibraltar",
		"continent": "eu",
		"es": "Gibraltar"
	},
	"DJI": {
		"alpha2": "DJ",
		"code": "afdji",
		"pt": "Yibuti",
		"en": "Djibouti",
		"fullname": "Djibouti",
		"continent": "af",
		"es": "Yibuti"
	},
	"GIN": {
		"alpha2": "GN",
		"code": "afgin",
		"pt": "Guiné",
		"en": "Guinea",
		"fullname": "Guinea",
		"continent": "af",
		"es": "Guinea"
	},
	"FIN": {
		"alpha2": "FI",
		"code": "eufin",
		"pt": "Finlândia",
		"en": "Finland",
		"fullname": "Finland",
		"continent": "eu",
		"es": "Finlandia"
	},
	"VAT": {
		"alpha2": "VA",
		"code": "euvat",
		"pt": "Ciudad del Vaticano",
		"en": "Vatican City State",
		"fullname": "Vatican city",
		"continent": "eu",
		"es": "Ciudad del Vaticano"
	},
	"SYC": {
		"alpha2": "SC",
		"code": "afsyc",
		"pt": "Seychelles",
		"en": "Seychelles",
		"fullname": "Seychelles",
		"continent": "af",
		"es": "Seychelles"
	},
	"NPL": {
		"alpha2": "NP",
		"code": "asnpl",
		"pt": "Nepal",
		"en": "Nepal",
		"fullname": "Nepal",
		"continent": "as",
		"es": "Nepal"
	},
	"MAR": {
		"alpha2": "MA",
		"code": "afmar",
		"pt": "Marrocos",
		"en": "Morocco",
		"fullname": "Morocco",
		"continent": "af",
		"es": "Marruecos"
	},
	"YEM": {
		"alpha2": "YE",
		"code": "asyem",
		"pt": "Iêmen",
		"en": "Yemen",
		"fullname": "Yemen",
		"continent": "as",
		"es": "Yemen"
	},
	"PHL": {
		"alpha2": "PH",
		"code": "asphl",
		"pt": "Filipinas",
		"en": "Philippines",
		"fullname": "Philippines",
		"continent": "as",
		"es": "Filipinas"
	},
	"ZAF": {
		"alpha2": "ZA",
		"code": "afzaf",
		"pt": "África do Sul",
		"en": "South Africa",
		"fullname": "South Africa",
		"continent": "af",
		"es": "Sudáfrica"
	},
	"NIC": {
		"alpha2": "NI",
		"code": "nanic",
		"pt": "Nicarágua",
		"en": "Nicaragua",
		"fullname": "Nicaragua",
		"continent": "na",
		"es": "Nicaragua"
	},
	"ROU": {
		"alpha2": "RO",
		"code": "eurou",
		"pt": "Romênia",
		"en": "Romania",
		"fullname": "Romania",
		"continent": "eu",
		"es": "Rumanía"
	},
	"SYR": {
		"alpha2": "SY",
		"code": "assyr",
		"pt": "Síria",
		"en": "Syria",
		"fullname": "Syria",
		"continent": "as",
		"es": "Siria"
	},
	"MAC": {
		"alpha2": "MO",
		"code": "asmac",
		"pt": "Macao",
		"en": "Macao",
		"fullname": "Macau",
		"continent": "as",
		"es": "Macao"
	},
	"NIU": {
		"alpha2": "NU",
		"code": "ocniu",
		"pt": "Niue",
		"en": "Niue",
		"fullname": "Niue",
		"continent": "oc",
		"es": "Niue"
	},
	"DMA": {
		"alpha2": "DM",
		"code": "nadma",
		"pt": "Dominica",
		"en": "Dominica",
		"fullname": "Dominica",
		"continent": "na",
		"es": "Dominica"
	},
	"BEN": {
		"alpha2": "BJ",
		"code": "afben",
		"pt": "Benin",
		"en": "Benin",
		"fullname": "Benin",
		"continent": "af",
		"es": "Benín"
	},
	"GUF": {
		"alpha2": "GY",
		"code": "saguf",
		"pt": "Guayana Francesa",
		"en": "French Guiana",
		"fullname": "Guiana",
		"continent": "sa",
		"es": "Guayana Francesa"
	},
	"BEL": {
		"alpha2": "BE",
		"code": "eubel",
		"pt": "Bélgica",
		"en": "Belgium",
		"fullname": "Belgium",
		"continent": "eu",
		"es": "Bélgica"
	},
	"GUM": {
		"alpha2": "GU",
		"code": "ocgum",
		"pt": "Guam",
		"en": "Guam",
		"fullname": "Guam",
		"continent": "oc",
		"es": "Guam"
	},
	"GBR": {
		"alpha2": "GB",
		"code": "eugbr",
		"pt": "Reino Unido",
		"en": "United Kingdom",
		"fullname": "United Kingdom",
		"continent": "eu",
		"es": "Reino Unido"
	},
	"BES": {
		"alpha2": "SE",
		"code": "nabes",
		"pt": "Sint Eustatius",
		"en": "Sint Eustatius",
		"fullname": "Sint Eustatius",
		"continent": "na",
		"es": "San Eustaquio"
	},
	"GUY": {
		"alpha2": "GY",
		"code": "saguy",
		"pt": "Guiana",
		"en": "Guyana",
		"fullname": "Guyana",
		"continent": "sa",
		"es": "Guyana"
	},
	"CRI": {
		"alpha2": "CR",
		"code": "nacri",
		"pt": "Costa Rica",
		"en": "Costa Rica",
		"fullname": "Costa Rica",
		"continent": "na",
		"es": "Costa Rica"
	},
	"CMR": {
		"alpha2": "CM",
		"code": "afcmr",
		"pt": "Camarões",
		"en": "Cameroon",
		"fullname": "Cameroon",
		"continent": "af",
		"es": "Camerún"
	},
	"LSO": {
		"alpha2": "LS",
		"code": "aflso",
		"pt": "Lesoto",
		"en": "Lesotho",
		"fullname": "Lesotho",
		"continent": "af",
		"es": "Lesoto"
	},
	"HUN": {
		"alpha2": "HU",
		"code": "euhun",
		"pt": "Hungria",
		"en": "Hungary",
		"fullname": "Hungary",
		"continent": "eu",
		"es": "Hungría"
	},
	"TTO": {
		"alpha2": "TT",
		"code": "natto",
		"pt": "Trinidad e Tobago",
		"en": "Trinidad and Tobago",
		"fullname": "Trinidad and Tobago",
		"continent": "na",
		"es": "Trinidad y Tobago"
	},
	"WLF": {
		"alpha2": "WF",
		"code": "ocwlf",
		"pt": "Wallis y Futuna",
		"en": "Wallis and Futuna",
		"fullname": "Wallis and Futuna Islands",
		"continent": "oc",
		"es": "Wallis y Futuna"
	},
	"TCD": {
		"alpha2": "TD",
		"code": "aftcd",
		"pt": "Chade",
		"en": "Chad",
		"fullname": "Chad Republic",
		"continent": "af",
		"es": "Chad"
	},
	"GEO": {
		"alpha2": "GE",
		"code": "asgeo",
		"pt": "Geórgia",
		"en": "Georgia",
		"fullname": "Georgia",
		"continent": "as",
		"es": "Georgia"
	},
	"SXM": {
		"alpha2": "SX",
		"fullname": "Sint Maarten",
		"code": "nasxm",
		"continent": "na"
	},
	"TCA": {
		"alpha2": "TC",
		"code": "natca",
		"pt": "Islas Turcas y Caicos",
		"en": "Turks and Caicos Islands",
		"fullname": "Turks and Caicos Islands",
		"continent": "na",
		"es": "Islas Turcas y Caicos"
	},
	"MHL": {
		"alpha2": "MH",
		"code": "ocmhl",
		"pt": "Islas Marshall",
		"en": "Marshall Islands",
		"fullname": "Marshall Islands",
		"continent": "oc",
		"es": "Islas Marshall"
	},
	"BLZ": {
		"alpha2": "BZ",
		"code": "nablz",
		"pt": "Belize",
		"en": "Belize",
		"fullname": "Belize",
		"continent": "na",
		"es": "Belice"
	},
	"NFK": {
		"alpha2": "NF",
		"code": "ocnfk",
		"pt": "Isla Norfolk",
		"en": "Norfolk Island",
		"fullname": "Norfolk Island",
		"continent": "oc",
		"es": "Isla Norfolk"
	},
	"VGB": {
		"alpha2": "VG",
		"fullname": "British Virgin Islands",
		"code": "navgb",
		"continent": "na"
	},
	"BLR": {
		"alpha2": "BY",
		"code": "eublr",
		"pt": "Belarus",
		"en": "Belarus",
		"fullname": "Belarus",
		"continent": "eu",
		"es": "Bielorrusia"
	},
	"GRD": {
		"alpha2": "GD",
		"code": "nagrd",
		"pt": "Granada",
		"en": "Grenada",
		"fullname": "Grenada and Carriacuou",
		"continent": "na",
		"es": "Granada"
	},
	"GRC": {
		"alpha2": "GR",
		"code": "eugrc",
		"pt": "Grécia",
		"en": "Greece",
		"fullname": "Greece",
		"continent": "eu",
		"es": "Grecia"
	},
	"GRL": {
		"alpha2": "GL",
		"code": "nagrl",
		"pt": "Groenlândia",
		"en": "Greenland",
		"fullname": "Greenland",
		"continent": "na",
		"es": "Groenlandia"
	},
	"AND": {
		"alpha2": "AD",
		"code": "euand",
		"pt": "Andorra",
		"en": "Andorra",
		"fullname": "Andorra",
		"continent": "eu",
		"es": "Andorra"
	},
	"MOZ": {
		"alpha2": "MZ",
		"code": "afmoz",
		"pt": "Moçambique",
		"en": "Mozambique",
		"fullname": "Mozambique",
		"continent": "af",
		"es": "Mozambique"
	},
	"TJK": {
		"alpha2": "TJ",
		"code": "astjk",
		"pt": "Tadjikistán",
		"en": "Tajikistan",
		"fullname": "Tajikistan",
		"continent": "as",
		"es": "Tadjikistán"
	},
	"PSE": {
		"alpha2": "PS",
		"code": "aspse",
		"pt": "Palestina",
		"en": "Palestine",
		"fullname": "Palestine",
		"continent": "as",
		"es": "Palestina"
	},
	"ANT": {
		"alpha2": "CW",
		"code": "naant",
		"pt": "Antillas Neerlandesas",
		"en": "Netherlands Antilles",
		"fullname": "Netherlands Antilles",
		"continent": "na",
		"es": "Antillas Neerlandesas"
	},
	"LCA": {
		"alpha2": "LC",
		"code": "nalca",
		"pt": "Santa Lucía",
		"en": "Saint Lucia",
		"fullname": "St. Lucia",
		"continent": "na",
		"es": "Santa Lucía"
	},
	"IND": {
		"alpha2": "IN",
		"code": "asind",
		"pt": "Índia",
		"en": "India",
		"fullname": "India",
		"continent": "as",
		"es": "India"
	},
	"NOR": {
		"alpha2": "NO",
		"code": "eunor",
		"pt": "Noruega",
		"en": "Norway",
		"fullname": "Norway",
		"continent": "eu",
		"es": "Noruega"
	},
	"FJI": {
		"alpha2": "FJ",
		"code": "ocfji",
		"pt": "Fiji",
		"en": "Fiji",
		"fullname": "Fiji Islands",
		"continent": "oc",
		"es": "Fiyi"
	},
	"HND": {
		"alpha2": "HN",
		"code": "nahnd",
		"pt": "Honduras",
		"en": "Honduras",
		"fullname": "Honduras",
		"continent": "na",
		"es": "Honduras"
	},
	"DOM": {
		"alpha2": "DO",
		"code": "nadom",
		"pt": "República Dominicana",
		"en": "Dominican Republic",
		"fullname": "Dominican Republic",
		"continent": "na",
		"es": "República Dominicana"
	},
	"FSM": {
		"alpha2": "FM",
		"code": "ocfsm",
		"pt": "Micronesia",
		"en": "Estados Federados de",
		"fullname": "Federated States of Micronesia",
		"continent": "oc",
		"es": "Micronesia"
	},
	"PER": {
		"alpha2": "PE",
		"code": "saper",
		"pt": "Peru",
		"en": "Peru",
		"fullname": "Peru",
		"continent": "sa",
		"es": "Perú"
	},
	"REU": {
		"alpha2": "RE",
		"code": "afreu",
		"pt": "Reunión",
		"en": "Réunion",
		"fullname": "Reunion Island",
		"continent": "af",
		"es": "Reunión"
	},
	"VUT": {
		"alpha2": "VU",
		"code": "ocvut",
		"pt": "Vanuatu",
		"en": "Vanuatu",
		"fullname": "Vanuatu",
		"continent": "oc",
		"es": "Vanuatu"
	},
	"GNQ": {
		"alpha2": "GQ",
		"code": "afgnq",
		"pt": "Guiné-Equatorial",
		"en": "Equatorial Guinea",
		"fullname": "Equatorial Guinea",
		"continent": "af",
		"es": "Guinea Ecuatorial"
	},
	"COD": {
		"alpha2": "CD",
		"code": "afcod",
		"pt": "Congo",
		"en": "Congo",
		"fullname": "Dem. Republic of the Congo",
		"continent": "af",
		"es": "Congo"
	},
	"COG": {
		"alpha2": "CG",
		"code": "afcog",
		"pt": "Congo",
		"en": "Congo",
		"fullname": "Congo",
		"continent": "af",
		"es": "Congo"
	},
	"GLP": {
		"alpha2": "GP",
		"code": "naglp",
		"pt": "Guadalupe",
		"en": "Guadeloupe",
		"fullname": "Guadeloupe",
		"continent": "na",
		"es": "Guadalupe"
	},
	"ETH": {
		"alpha2": "ET",
		"code": "afeth",
		"pt": "Etiópia",
		"en": "Ethiopia",
		"fullname": "Ethiopia",
		"continent": "af",
		"es": "Etiopía"
	},
	"COM": {
		"alpha2": "KM",
		"code": "afcom",
		"pt": "Comoras",
		"en": "Comoros",
		"fullname": "Comoros",
		"continent": "af",
		"es": "Comoras"
	},
	"COL": {
		"alpha2": "CO",
		"code": "sacol",
		"pt": "Colombia",
		"en": "Colombia",
		"fullname": "Colombia",
		"continent": "sa",
		"es": "Colombia"
	},
	"MDA": {
		"alpha2": "MD",
		"code": "eumda",
		"pt": "Moldavia",
		"en": "Moldova",
		"fullname": "Moldova",
		"continent": "eu",
		"es": "Moldavia"
	},
	"MDG": {
		"alpha2": "MG",
		"code": "afmdg",
		"pt": "Madagascar",
		"en": "Madagascar",
		"fullname": "Madagascar",
		"continent": "af",
		"es": "Madagascar"
	},
	"MDV": {
		"alpha2": "MV",
		"code": "asmdv",
		"pt": "Islas Maldivas",
		"en": "Maldives",
		"fullname": "Maldives",
		"continent": "as",
		"es": "Islas Maldivas"
	},
	"SRB": {
		"alpha2": "RS",
		"code": "eusrb",
		"pt": "Serbia",
		"en": "Serbia",
		"fullname": "Serbia",
		"continent": "eu",
		"es": "Serbia"
	},
	"LTU": {
		"alpha2": "LT",
		"code": "eultu",
		"pt": "Lituânia",
		"en": "Lithuania",
		"fullname": "Lithuania",
		"continent": "eu",
		"es": "Lituania"
	},
	"RWA": {
		"alpha2": "RW",
		"code": "afrwa",
		"pt": "Ruanda",
		"en": "Rwanda",
		"fullname": "Rwanda",
		"continent": "af",
		"es": "Ruanda"
	},
	"ZMB": {
		"alpha2": "ZM",
		"code": "afzmb",
		"pt": "Zâmbia",
		"en": "Zambia",
		"fullname": "Zambia",
		"continent": "af",
		"es": "Zambia"
	},
	"SWE": {
		"alpha2": "SE",
		"code": "euswe",
		"pt": "Suécia",
		"en": "Sweden",
		"fullname": "Sweden",
		"continent": "eu",
		"es": "Suecia"
	},
	"MSR": {
		"alpha2": "MS",
		"code": "namsr",
		"pt": "Montserrat",
		"en": "Montserrat",
		"fullname": "Montserrat",
		"continent": "na",
		"es": "Montserrat"
	},
	"AUS": {
		"alpha2": "AU",
		"code": "ocaus",
		"pt": "Austrália",
		"en": "Australia",
		"fullname": "Australia",
		"continent": "oc",
		"es": "Australia"
	},
	"AUT": {
		"alpha2": "AT",
		"code": "euaut",
		"pt": "Áustria",
		"en": "Austria",
		"fullname": "Austria",
		"continent": "eu",
		"es": "Austria"
	},
	"VEN": {
		"alpha2": "VE",
		"code": "saven",
		"pt": "Venezuela",
		"en": "Venezuela",
		"fullname": "Venezuela",
		"continent": "sa",
		"es": "Venezuela"
	},
	"PLW": {
		"alpha2": "PW",
		"code": "ocplw",
		"pt": "Palau",
		"en": "Palau",
		"fullname": "Palau",
		"continent": "oc",
		"es": "Palau"
	},
	"ALB": {
		"alpha2": "AL",
		"code": "eualb",
		"pt": "Albânia",
		"en": "Albania",
		"fullname": "Albania",
		"continent": "eu",
		"es": "Albania"
	},
	"MMR": {
		"alpha2": "MM",
		"code": "asmmr",
		"pt": "Birmania",
		"en": "Myanmar",
		"fullname": "Myanmar",
		"continent": "as",
		"es": "Birmania"
	},
	"RUS": {
		"alpha2": "RU",
		"code": "asrus",
		"pt": "Rússia",
		"en": "Russia",
		"fullname": "Russia",
		"continent": "eu",
		"es": "Rusia"
	},
	"MKD": {
		"alpha2": "MK",
		"code": "eumkd",
		"pt": "Macedonia",
		"en": "Macedonia",
		"fullname": "Macedonia",
		"continent": "eu",
		"es": "Macedonia"
	},
	"WSM": {
		"alpha2": "WS",
		"code": "ocwsm",
		"pt": "Samoa",
		"en": "Samoa",
		"fullname": "Samoa",
		"continent": "oc",
		"es": "Samoa"
	},
	"UKR": {
		"alpha2": "UA",
		"code": "euukr",
		"pt": "Ucrânia",
		"en": "Ukraine",
		"fullname": "Ukraine",
		"continent": "eu",
		"es": "Ucrania"
	},
	"GNB": {
		"alpha2": "GW",
		"code": "afgnb",
		"pt": "Guiné-Bissau",
		"en": "Guinea-Bissau",
		"fullname": "Guinea-Bissau",
		"continent": "af",
		"es": "Guinea-Bissau"
	},
	"TON": {
		"alpha2": "TO",
		"code": "octon",
		"pt": "Tonga",
		"en": "Tonga",
		"fullname": "Tonga",
		"continent": "oc",
		"es": "Tonga"
	},
	"CAN": {
		"alpha2": "CA",
		"code": "nacan",
		"pt": "Canadá",
		"en": "Canada",
		"fullname": "Canada",
		"continent": "na",
		"es": "Canadá"
	},
	"KOR": {
		"alpha2": "KR",
		"code": "askor",
		"pt": "Coréia do Sul",
		"en": "South Korea",
		"fullname": "South Korea",
		"continent": "as",
		"es": "Corea del Sur"
	},
	"AIA": {
		"alpha2": "AI",
		"code": "naaia",
		"pt": "Anguila",
		"en": "Anguilla",
		"fullname": "Anguilla",
		"continent": "na",
		"es": "Anguila"
	},
	"CAF": {
		"alpha2": "CF",
		"code": "afcaf",
		"pt": "República Centro-Africana",
		"en": "Central African Republic",
		"fullname": "Central African Republic",
		"continent": "af",
		"es": "República Centroafricana"
	},
	"SVK": {
		"alpha2": "SK",
		"code": "eusvk",
		"pt": "Eslováquia",
		"en": "Slovakia",
		"fullname": "Slovakia",
		"continent": "eu",
		"es": "Eslovaquia"
	},
	"SOM": {
		"alpha2": "SO",
		"code": "afsom",
		"pt": "Somália",
		"en": "Somalia",
		"fullname": "Somalia Republic",
		"continent": "af",
		"es": "Somalia"
	},
	"ERI": {
		"alpha2": "ER",
		"code": "aferi",
		"pt": "Eritrea",
		"en": "Eritrea",
		"fullname": "Eritrea",
		"continent": "af",
		"es": "Eritrea"
	},
	"GAB": {
		"alpha2": "GA",
		"code": "afgab",
		"pt": "Gabão",
		"en": "Gabon",
		"fullname": "Gabon Republic",
		"continent": "af",
		"es": "Gabón"
	},
	"IRQ": {
		"alpha2": "IQ",
		"code": "asirq",
		"pt": "Iraque",
		"en": "Iraq",
		"fullname": "Iraq",
		"continent": "as",
		"es": "Irak"
	},
	"MTQ": {
		"alpha2": "MQ",
		"code": "namtq",
		"pt": "Martinica",
		"en": "Martinique",
		"fullname": "Martinique",
		"continent": "na",
		"es": "Martinica"
	},
	"LVA": {
		"alpha2": "LV",
		"code": "eulva",
		"pt": "Letonia",
		"en": "Latvia",
		"fullname": "Latvia",
		"continent": "eu",
		"es": "Letonia"
	},
	"IRN": {
		"alpha2": "IR",
		"code": "asirn",
		"pt": "Irã",
		"en": "Iran",
		"fullname": "Iran",
		"continent": "as",
		"es": "Irán"
	},
	"ABW": {
		"alpha2": "AW",
		"code": "naabw",
		"pt": "Aruba",
		"en": "Aruba",
		"fullname": "Aruba",
		"continent": "na",
		"es": "Aruba"
	},
	"NZL": {
		"alpha2": "NZ",
		"code": "ocnzl",
		"pt": "Nova Zelândia",
		"en": "New Zealand",
		"fullname": "New Zealand",
		"continent": "oc",
		"es": "Nueva Zelanda"
	},
	"RSB": {
		"alpha2": "RS",
		"fullname": "Yugoslavia",
		"code": "eursb",
		"continent": "eu"
	},
	"NCL": {
		"alpha2": "NC",
		"code": "ocncl",
		"pt": "Nova Caledônia",
		"en": "New Caledonia",
		"fullname": "New Caledonia",
		"continent": "oc",
		"es": "Nueva Caledonia"
	},
	"ARE": {
		"alpha2": "AE",
		"code": "asare",
		"pt": "Emirados Árabes Unidos",
		"en": "United Arab Emirates",
		"fullname": "United Arab Emirates",
		"continent": "as",
		"es": "Emiratos Árabes Unidos"
	},
	"ARG": {
		"alpha2": "AR",
		"code": "saarg",
		"pt": "Argentina",
		"en": "Argentina",
		"fullname": "Argentina",
		"continent": "sa",
		"es": "Argentina"
	},
	"BHS": {
		"alpha2": "BS",
		"code": "nabhs",
		"pt": "Bahamas",
		"en": "Bahamas",
		"fullname": "Bahamas",
		"continent": "na",
		"es": "Bahamas"
	},
	"BHR": {
		"alpha2": "BH",
		"code": "asbhr",
		"pt": "Bahrein",
		"en": "Bahrain",
		"fullname": "Bahrain",
		"continent": "as",
		"es": "Bahrein"
	},
	"ARM": {
		"alpha2": "AM",
		"code": "asarm",
		"pt": "Armênia",
		"en": "Armenia",
		"fullname": "Armenia",
		"continent": "as",
		"es": "Armenia"
	},
	"PNG": {
		"alpha2": "PG",
		"code": "ocpng",
		"pt": "Papua-Nova-Guiné",
		"en": "Papua New Guinea",
		"fullname": "Papua New Guinea",
		"continent": "oc",
		"es": "Papúa Nueva Guinea"
	},
	"LIE": {
		"alpha2": "LI",
		"code": "eulie",
		"pt": "Liechtenstein",
		"en": "Liechtenstein",
		"fullname": "Liechtenstein",
		"continent": "eu",
		"es": "Liechtenstein"
	},
	"EGY": {
		"alpha2": "EG",
		"code": "afegy",
		"pt": "Egito",
		"en": "Egypt",
		"fullname": "Egypt",
		"continent": "af",
		"es": "Egipto"
	},
	"NAM": {
		"alpha2": "NA",
		"code": "afnam",
		"pt": "Namíbia",
		"en": "Namibia",
		"fullname": "Namibia",
		"continent": "af",
		"es": "Namibia"
	},
	"BOL": {
		"alpha2": "BO",
		"code": "sabol",
		"pt": "Bolívia",
		"en": "Bolivia",
		"fullname": "Bolivia",
		"continent": "sa",
		"es": "Bolivia"
	},
	"GHA": {
		"alpha2": "GH",
		"code": "afgha",
		"pt": "Gana",
		"en": "Ghana",
		"fullname": "Ghana",
		"continent": "af",
		"es": "Ghana"
	},
	"CCK": {
		"alpha2": "CC",
		"code": "ascck",
		"pt": "Islas Cocos (Keeling)",
		"en": "Cocos (Keeling) Islands",
		"fullname": "Cocos Islands",
		"continent": "as",
		"es": "Islas Cocos (Keeling)"
	},
	"JOR": {
		"alpha2": "JO",
		"code": "asjor",
		"pt": "Jordânia",
		"en": "Jordan",
		"fullname": "Jordan",
		"continent": "as",
		"es": "Jordania"
	},
	"LBR": {
		"alpha2": "LR",
		"code": "aflbr",
		"pt": "Libéria",
		"en": "Liberia",
		"fullname": "Liberia",
		"continent": "af",
		"es": "Liberia"
	},
	"LBY": {
		"alpha2": "LY",
		"code": "aflby",
		"pt": "Libia",
		"en": "Libya",
		"fullname": "Libya",
		"continent": "af",
		"es": "Libia"
	},
	"MYS": {
		"alpha2": "MY",
		"code": "asmys",
		"pt": "Malásia",
		"en": "Malaysia",
		"fullname": "Malaysia",
		"continent": "as",
		"es": "Malasia"
	},
	"IOT": {
		"alpha2": "IO",
		"code": "asiot",
		"pt": "Territorio Británico del Océano Índico",
		"en": "British Indian Ocean Territory",
		"fullname": "Diego Garcia",
		"continent": "as",
		"es": "Territorio Británico del Océano Índico"
	},
	"PRI": {
		"alpha2": "PR",
		"code": "napri",
		"pt": "Porto Rico",
		"en": "Puerto Rico",
		"fullname": "Puerto Rico",
		"continent": "na",
		"es": "Puerto Rico"
	},
	"MYT": {
		"alpha2": "YT",
		"code": "afmyt",
		"pt": "Mayotte",
		"en": "Mayotte",
		"fullname": "Mayotte Island",
		"continent": "af",
		"es": "Mayotte"
	},
	"PRK": {
		"alpha2": "KP",
		"code": "asprk",
		"pt": "Coréia do Norte",
		"en": "North Korea",
		"fullname": "North Korea",
		"continent": "as",
		"es": "Corea del Norte"
	},
	"PRT": {
		"alpha2": "PT",
		"code": "euprt",
		"pt": "Portugal",
		"en": "Portugal",
		"fullname": "Portugal",
		"continent": "eu",
		"es": "Portugal"
	},
	"KHM": {
		"alpha2": "KH",
		"code": "askhm",
		"pt": "Camboja",
		"en": "Cambodia",
		"fullname": "Cambodia",
		"continent": "as",
		"es": "Camboya"
	},
	"PRY": {
		"alpha2": "PY",
		"code": "sapry",
		"pt": "Paraguai",
		"en": "Paraguay",
		"fullname": "Paraguay",
		"continent": "sa",
		"es": "Paraguay"
	},
	"HKG": {
		"alpha2": "HK",
		"code": "ashkg",
		"pt": "Hong Kong",
		"en": "Hong Kong",
		"fullname": "Hong Kong",
		"continent": "as",
		"es": "Hong kong"
	},
	"SAU": {
		"alpha2": "SA",
		"code": "assau",
		"pt": "Arábia Saudita",
		"en": "Saudi Arabia",
		"fullname": "Saudi Arabia",
		"continent": "as",
		"es": "Arabia Saudita"
	},
	"LBN": {
		"alpha2": "LB",
		"code": "aslbn",
		"pt": "Líbano",
		"en": "Lebanon",
		"fullname": "Lebanon",
		"continent": "as",
		"es": "Líbano"
	},
	"SVN": {
		"alpha2": "SI",
		"code": "eusvn",
		"pt": "Eslovênia",
		"en": "Slovenia",
		"fullname": "Slovenia",
		"continent": "eu",
		"es": "Eslovenia"
	},
	"BFA": {
		"alpha2": "BF",
		"code": "afbfa",
		"pt": "Burquina Faso",
		"en": "Burkina Faso",
		"fullname": "Burkina Faso",
		"continent": "af",
		"es": "Burkina Faso"
	},
	"CHE": {
		"alpha2": "CH",
		"code": "euche",
		"pt": "Suíça",
		"en": "Switzerland",
		"fullname": "Switzerland",
		"continent": "eu",
		"es": "Suiza"
	},
	"MRT": {
		"alpha2": "MR",
		"code": "afmrt",
		"pt": "Mauritânia",
		"en": "Mauritania",
		"fullname": "Mauritania",
		"continent": "af",
		"es": "Mauritania"
	},
	"HRV": {
		"alpha2": "HR",
		"code": "euhrv",
		"pt": "Croácia",
		"en": "Croatia",
		"fullname": "Croatia",
		"continent": "eu",
		"es": "Croacia"
	},
	"CHL": {
		"alpha2": "CL",
		"code": "sachl",
		"pt": "Chile",
		"en": "Chile",
		"fullname": "Chile",
		"continent": "sa",
		"es": "Chile"
	},
	"CHN": {
		"alpha2": "CN",
		"code": "aschn",
		"pt": "China",
		"en": "China",
		"fullname": "China",
		"continent": "as",
		"es": "China"
	},
	"SMR": {
		"alpha2": "SM",
		"code": "eusmr",
		"pt": "San Marino",
		"en": "San Marino",
		"fullname": "San Marino",
		"continent": "eu",
		"es": "San Marino"
	},
	"JAM": {
		"alpha2": "JM",
		"code": "najam",
		"pt": "Jamaica",
		"en": "Jamaica",
		"fullname": "Jamaica",
		"continent": "na",
		"es": "Jamaica"
	},
	"URY": {
		"alpha2": "UY",
		"code": "saury",
		"pt": "Uruguai",
		"en": "Uruguay",
		"fullname": "Uruguay",
		"continent": "sa",
		"es": "Uruguay"
	},
	"CXR": {
		"alpha2": "CX",
		"code": "ascxr",
		"pt": "Isla de Navidad",
		"en": "Christmas Island",
		"fullname": "Christmas Island",
		"continent": "as",
		"es": "Isla de Navidad"
	},
	"VIR": {
		"alpha2": "VI",
		"code": "navir",
		"pt": "Islas Vírgenes de los Estados Unidos",
		"en": "United States Virgin Islands",
		"fullname": "US Virgin Islands",
		"continent": "na",
		"es": "Islas Vírgenes de los Estados Unidos"
	},
	"KIR": {
		"alpha2": "KI",
		"code": "ockir",
		"pt": "Kiribati",
		"en": "Kiribati",
		"fullname": "Kiribati",
		"continent": "oc",
		"es": "Kiribati"
	},
	"KAZ": {
		"alpha2": "KZ",
		"code": "askaz",
		"pt": "Cazaquistão",
		"en": "Kazakhstan",
		"fullname": "Kazakhstan",
		"continent": "as",
		"es": "Kazajistán"
	},
	"PYF": {
		"alpha2": "PF",
		"code": "ocpyf",
		"pt": "Polinésia Francesa",
		"en": "French Polynesia",
		"fullname": "French Polynesia",
		"continent": "oc",
		"es": "Polinesia Francesa"
	},
	"NGA": {
		"alpha2": "NG",
		"code": "afnga",
		"pt": "Nigéria",
		"en": "Nigeria",
		"fullname": "Nigeria",
		"continent": "af",
		"es": "Nigeria"
	},
	"DEU": {
		"alpha2": "DE",
		"code": "eudeu",
		"pt": "Alemanha",
		"en": "Germany",
		"fullname": "Germany",
		"continent": "eu",
		"es": "Alemania"
	},
	"LKA": {
		"alpha2": "LK",
		"code": "aslka",
		"pt": "Sri Lanka",
		"en": "Sri Lanka",
		"fullname": "Sri Lanka",
		"continent": "as",
		"es": "Sri lanka"
	},
	"FLK": {
		"alpha2": "FK",
		"code": "saflk",
		"pt": "Islas Malvinas",
		"en": "Falkland Islands (Malvinas)",
		"fullname": "Falkland Islands",
		"continent": "sa",
		"es": "Islas Malvinas"
	},
	"MWI": {
		"alpha2": "MW",
		"code": "afmwi",
		"pt": "Malawi",
		"en": "Malawi",
		"fullname": "Malawi",
		"continent": "af",
		"es": "Malawi"
	},
	"COK": {
		"alpha2": "CK",
		"code": "occok",
		"pt": "Ilhas Cook",
		"en": "Cook Islands",
		"fullname": "Cook Islands",
		"continent": "oc",
		"es": "Islas Cook"
	},
	"MNP": {
		"alpha2": "MP",
		"code": "ocmnp",
		"pt": "Islas Marianas del Norte",
		"en": "Northern Mariana Islands",
		"fullname": "Saipan",
		"continent": "oc",
		"es": "Islas Marianas del Norte"
	},
	"UMI": {
		"alpha2": "UM",
		"code": "ocumi",
		"pt": "Islas Ultramarinas Menores de Estados Unidos",
		"en": "United States Minor Outlying Islands",
		"fullname": "Wake Island",
		"continent": "oc",
		"es": "Islas Ultramarinas Menores de Estados Unidos"
	},
	"UGA": {
		"alpha2": "UG",
		"code": "afuga",
		"pt": "Uganda",
		"en": "Uganda",
		"fullname": "Uganda",
		"continent": "af",
		"es": "Uganda"
	},
	"TKM": {
		"alpha2": "TM",
		"code": "astkm",
		"pt": "Turkmenistán",
		"en": "Turkmenistan",
		"fullname": "Turkmenistan",
		"continent": "as",
		"es": "Turkmenistán"
	},
	"NLD": {
		"alpha2": "NL",
		"code": "eunld",
		"pt": "Países Baixos",
		"en": "Netherlands",
		"fullname": "Netherlands",
		"continent": "eu",
		"es": "Países Bajos"
	},
	"BMU": {
		"alpha2": "BM",
		"code": "nabmu",
		"pt": "Islas Bermudas",
		"en": "Bermuda Islands",
		"fullname": "Bermuda",
		"continent": "na",
		"es": "Islas Bermudas"
	},
	"MNE": {
		"alpha2": "ME",
		"code": "eumne",
		"pt": "Montenegro",
		"en": "Montenegro",
		"fullname": "Montenegro",
		"continent": "eu",
		"es": "Montenegro"
	},
	"MNG": {
		"alpha2": "MN",
		"code": "asmng",
		"pt": "Mongólia",
		"en": "Mongolia",
		"fullname": "Mongolia",
		"continent": "as",
		"es": "Mongolia"
	},
	"AFG": {
		"alpha2": "AF",
		"code": "asafg",
		"pt": "Afeganistão",
		"en": "Afghanistan",
		"fullname": "Afghanistan",
		"continent": "as",
		"es": "Afganistán"
	},
	"BDI": {
		"alpha2": "BI",
		"code": "afbdi",
		"pt": "Burundi",
		"en": "Burundi",
		"fullname": "Burundi",
		"continent": "af",
		"es": "Burundi"
	},
	"SHN": {
		"alpha2": "SH",
		"code": "afshn",
		"pt": "Santa Elena",
		"en": "Ascensión y Tristán de Acuña",
		"fullname": "St. Helena",
		"continent": "af",
		"es": "Santa Elena"
	},
	"THA": {
		"alpha2": "TH",
		"code": "astha",
		"pt": "Tailândia",
		"en": "Thailand",
		"fullname": "Thailand",
		"continent": "as",
		"es": "Tailandia"
	},
	"HTI": {
		"alpha2": "HT",
		"code": "nahti",
		"pt": "Haiti",
		"en": "Haiti",
		"fullname": "Haiti",
		"continent": "na",
		"es": "Haití"
	},
	"STP": {
		"alpha2": "ST",
		"code": "afstp",
		"pt": "Santo Tomé y Príncipe",
		"en": "Sao Tome and Principe",
		"fullname": "Sao Tome",
		"continent": "af",
		"es": "Santo Tomé y Príncipe"
	},
	"BTN": {
		"alpha2": "BT",
		"code": "asbtn",
		"pt": "Butão",
		"en": "Bhutan",
		"fullname": "Bhutan",
		"continent": "as",
		"es": "Bhután"
	},
	"CZE": {
		"alpha2": "CZ",
		"code": "eucze",
		"pt": "República Tcheca",
		"en": "Czech Republic",
		"fullname": "Czech Republic",
		"continent": "eu",
		"es": "República Checa"
	},
	"ATG": {
		"alpha2": "AG",
		"code": "naatg",
		"pt": "Antigua y Barbuda",
		"en": "Antigua and Barbuda",
		"fullname": "Antigua and Barbuda",
		"continent": "na",
		"es": "Antigua y Barbuda"
	},
	"MUS": {
		"alpha2": "MU",
		"code": "afmus",
		"pt": "Mauricio",
		"en": "Mauritius",
		"fullname": "Mauritius",
		"continent": "af",
		"es": "Mauricio"
	},
	"ATA": {
		"alpha2": "AQ",
		"code": "anata",
		"pt": "Antártida",
		"en": "Antarctica",
		"fullname": "Antarctica",
		"continent": "an",
		"es": "Antártida"
	},
	"LUX": {
		"alpha2": "LU",
		"code": "eulux",
		"pt": "Luxemburgo",
		"en": "Luxembourg",
		"fullname": "Luxembourg",
		"continent": "eu",
		"es": "Luxemburgo"
	},
	"ISR": {
		"alpha2": "IL",
		"code": "asisr",
		"pt": "Israel",
		"en": "Israel",
		"fullname": "Israel",
		"continent": "as",
		"es": "Israel"
	},
	"IDN": {
		"alpha2": "ID",
		"code": "asidn",
		"pt": "Indonésia",
		"en": "Indonesia",
		"fullname": "Indonesia",
		"continent": "as",
		"es": "Indonesia"
	},
	"SUR": {
		"alpha2": "SR",
		"code": "sasur",
		"pt": "Surinám",
		"en": "Suriname",
		"fullname": "Suriname",
		"continent": "sa",
		"es": "Surinám"
	},
	"ISL": {
		"alpha2": "IS",
		"code": "euisl",
		"pt": "Islândia",
		"en": "Iceland",
		"fullname": "Iceland",
		"continent": "eu",
		"es": "Islandia"
	},
	"NER": {
		"alpha2": "NE",
		"code": "afner",
		"pt": "Níger",
		"en": "Niger",
		"fullname": "Niger Republic",
		"continent": "af",
		"es": "Niger"
	},
	"ECU": {
		"alpha2": "EC",
		"code": "saecu",
		"pt": "Equador",
		"en": "Ecuador",
		"fullname": "Ecuador",
		"continent": "sa",
		"es": "Ecuador"
	},
	"SEN": {
		"alpha2": "SN",
		"code": "afsen",
		"pt": "Senegal",
		"en": "Senegal",
		"fullname": "Senegal Republic",
		"continent": "af",
		"es": "Senegal"
	},
	"ASM": {
		"alpha2": "AS",
		"code": "ocasm",
		"pt": "Samoa Americana",
		"en": "American Samoa",
		"fullname": "American Samoa",
		"continent": "oc",
		"es": "Samoa Americana"
	},
	"CUW": {
		"alpha2": "CW",
		"fullname": "Curacao",
		"code": "nacuw",
		"continent": "na"
	},
	"FRA": {
		"alpha2": "FR",
		"code": "eufra",
		"pt": "França",
		"en": "France",
		"fullname": "France",
		"continent": "eu",
		"es": "Francia"
	},
	"GMB": {
		"alpha2": "GM",
		"code": "afgmb",
		"pt": "Gambia",
		"en": "Gambia",
		"fullname": "Gambia",
		"continent": "af",
		"es": "Gambia"
	},
	"SSD": {
		"alpha2": "SS",
		"fullname": "South Sudan",
		"code": "afssd",
		"continent": "af"
	},
	"IRL": {
		"alpha2": "IE",
		"code": "euirl",
		"pt": "Irlanda",
		"en": "Ireland",
		"fullname": "Ireland",
		"continent": "eu",
		"es": "Irlanda"
	},
	"GTM": {
		"alpha2": "GT",
		"code": "nagtm",
		"pt": "Guatemala",
		"en": "Guatemala",
		"fullname": "Guatemala",
		"continent": "na",
		"es": "Guatemala"
	},
	"DNK": {
		"alpha2": "DK",
		"code": "eudnk",
		"pt": "Dinamarca",
		"en": "Denmark",
		"fullname": "Denmark",
		"continent": "eu",
		"es": "Dinamarca"
	},
	"ZWE": {
		"alpha2": "ZW",
		"code": "afzwe",
		"pt": "Zimbábue",
		"en": "Zimbabwe",
		"fullname": "Zimbabwe",
		"continent": "af",
		"es": "Zimbabue"
	},
	"CUB": {
		"alpha2": "CU",
		"code": "nacub",
		"pt": "Cuba",
		"en": "Cuba",
		"fullname": "Cuba",
		"continent": "na",
		"es": "Cuba"
	},
	"KEN": {
		"alpha2": "KE",
		"code": "afken",
		"pt": "Quênia",
		"en": "Kenya",
		"fullname": "Kenya",
		"continent": "af",
		"es": "Kenia"
	},
	"LAO": {
		"alpha2": "LA",
		"code": "aslao",
		"pt": "Laos",
		"en": "Laos",
		"fullname": "Laos",
		"continent": "as",
		"es": "Laos"
	},
	"TUR": {
		"alpha2": "TR",
		"code": "astur",
		"pt": "Turquia",
		"en": "Turkey",
		"fullname": "Turkey",
		"continent": "as",
		"es": "Turquía"
	},
	"JPN": {
		"alpha2": "JP",
		"code": "asjpn",
		"pt": "Japão",
		"en": "Japan",
		"fullname": "Japan",
		"continent": "as",
		"es": "Japón"
	},
	"OMN": {
		"alpha2": "OM",
		"code": "asomn",
		"pt": "Omã",
		"en": "Oman",
		"fullname": "Oman",
		"continent": "as",
		"es": "Omán"
	},
	"TUV": {
		"alpha2": "TV",
		"code": "octuv",
		"pt": "Tuvalu",
		"en": "Tuvalu",
		"fullname": "Tuvalu",
		"continent": "oc",
		"es": "Tuvalu"
	},
	"ITA": {
		"alpha2": "IT",
		"code": "euita",
		"pt": "Itália",
		"en": "Italy",
		"fullname": "Italy",
		"continent": "eu",
		"es": "Italia"
	},
	"BRN": {
		"alpha2": "BN",
		"code": "asbrn",
		"pt": "Brunei",
		"en": "Brunei",
		"fullname": "Brunei",
		"continent": "as",
		"es": "Brunéi"
	},
	"TUN": {
		"alpha2": "TN",
		"code": "aftun",
		"pt": "Tunísia",
		"en": "Tunisia",
		"fullname": "Tunisia",
		"continent": "af",
		"es": "Tunez"
	},
	"MEX": {
		"alpha2": "MX",
		"code": "namex",
		"pt": "México",
		"en": "Mexico",
		"fullname": "Mexico",
		"continent": "na",
		"es": "México"
	},
	"BRA": {
		"alpha2": "BR",
		"code": "sabra",
		"pt": "Brasil",
		"en": "Brazil",
		"fullname": "Brazil",
		"continent": "sa",
		"es": "Brasil"
	},
	"TLS": {
		"alpha2": "TL",
		"code": "octls",
		"pt": "Timor Oriental",
		"en": "East Timor",
		"fullname": "East Timor",
		"continent": "oc",
		"es": "Timor Oriental"
	},
	"USA": {
		"alpha2": "US",
		"code": "nausa",
		"pt": "Estados Unidos de América",
		"en": "United States of America",
		"fullname": "United States",
		"continent": "na",
		"es": "Estados Unidos de América"
	},
	"TWN": {
		"alpha2": "TW",
		"code": "astwn",
		"pt": "Taiwan",
		"en": "Taiwan",
		"fullname": "Taiwan",
		"continent": "as",
		"es": "Taiwán"
	},
	"AZE": {
		"alpha2": "AZ",
		"code": "asaze",
		"pt": "Azerbaijão",
		"en": "Azerbaijan",
		"fullname": "Azerbaijan",
		"continent": "as",
		"es": "Azerbayán"
	},
	"SWZ": {
		"alpha2": "SZ",
		"code": "afswz",
		"pt": "Suazilândia",
		"en": "Swaziland",
		"fullname": "Swaziland",
		"continent": "af",
		"es": "Swazilandia"
	},
	"CIV": {
		"alpha2": "CI",
		"code": "afciv",
		"pt": "Costa do Marfim",
		"en": "Ivory Coast",
		"fullname": "Ivory Coast",
		"continent": "af",
		"es": "Costa de Marfil"
	},
	"CYP": {
		"alpha2": "CY",
		"code": "eucyp",
		"pt": "Chipre",
		"en": "Cyprus",
		"fullname": "Cyprus",
		"continent": "eu",
		"es": "Chipre"
	},
	"BIH": {
		"alpha2": "BA",
		"code": "eubih",
		"pt": "Bosnia y Herzegovina",
		"en": "Bosnia and Herzegovina",
		"fullname": "Bosnia and Herzegovina",
		"continent": "eu",
		"es": "Bosnia y Herzegovina"
	},
	"SGP": {
		"alpha2": "SG",
		"code": "assgp",
		"pt": "Cingapura",
		"en": "Singapore",
		"fullname": "Singapore",
		"continent": "as",
		"es": "Singapur"
	},
	"UZB": {
		"alpha2": "UZ",
		"code": "asuzb",
		"pt": "Uzbekistán",
		"en": "Uzbekistan",
		"fullname": "Uzbekistan",
		"continent": "as",
		"es": "Uzbekistán"
	},
	"POL": {
		"alpha2": "PL",
		"code": "eupol",
		"pt": "Polônia",
		"en": "Poland",
		"fullname": "Poland",
		"continent": "eu",
		"es": "Polonia"
	},
	"KWT": {
		"alpha2": "KW",
		"code": "askwt",
		"pt": "Kuait",
		"en": "Kuwait",
		"fullname": "Kuwait",
		"continent": "as",
		"es": "Kuwait"
	},
	"TGO": {
		"alpha2": "TG",
		"code": "aftgo",
		"pt": "Togo",
		"en": "Togo",
		"fullname": "Togo",
		"continent": "af",
		"es": "Togo"
	},
	"CYM": {
		"alpha2": "KY",
		"code": "nacym",
		"pt": "Ilhas Cayman",
		"en": "Cayman Islands",
		"fullname": "Cayman Islands",
		"continent": "na",
		"es": "Islas Caimán"
	},
	"EST": {
		"alpha2": "EE",
		"code": "euest",
		"pt": "Estônia",
		"en": "Estonia",
		"fullname": "Estonia",
		"continent": "eu",
		"es": "Estonia"
	},
	"ESP": {
		"alpha2": "ES",
		"code": "euesp",
		"pt": "Espanha",
		"en": "Spain",
		"fullname": "Spain",
		"continent": "eu",
		"es": "España"
	},
	"FRO": {
		"alpha2": "FO",
		"code": "eufro",
		"pt": "Islas Feroe",
		"en": "Faroe Islands",
		"fullname": "Faroe Islands",
		"continent": "eu",
		"es": "Islas Feroe"
	},
	"SLV": {
		"alpha2": "SV",
		"code": "naslv",
		"pt": "El Salvador",
		"en": "El Salvador",
		"fullname": "El Salvador",
		"continent": "na",
		"es": "El Salvador"
	},
	"MLI": {
		"alpha2": "ML",
		"code": "afmli",
		"pt": "Mali",
		"en": "Mali",
		"fullname": "Mali Republic",
		"continent": "af",
		"es": "Mali"
	},
	"VCT": {
		"alpha2": "VC",
		"code": "navct",
		"pt": "San Vicente y las Granadinas",
		"en": "Saint Vincent and the Grenadines",
		"fullname": "St. Vincent",
		"continent": "na",
		"es": "San Vicente y las Granadinas"
	},
	"MLT": {
		"alpha2": "MT",
		"code": "eumlt",
		"pt": "Malta",
		"en": "Malta",
		"fullname": "Malta",
		"continent": "eu",
		"es": "Malta"
	},
	"SLE": {
		"alpha2": "SL",
		"code": "afsle",
		"pt": "Sierra Leona",
		"en": "Sierra Leone",
		"fullname": "Sierra Leone",
		"continent": "af",
		"es": "Sierra Leona"
	},
	"IMN": {
		"alpha2": "IM",
		"code": "euimn",
		"pt": "Ilha de Man",
		"en": "Isle of Man",
		"fullname": "Isle of Man",
		"continent": "eu",
		"es": "Isla de Man"
	},
	"PAN": {
		"alpha2": "PA",
		"code": "napan",
		"pt": "Panamá",
		"en": "Panama",
		"fullname": "Panama",
		"continent": "na",
		"es": "Panamá"
	},
	"SDN": {
		"alpha2": "SD",
		"code": "afsdn",
		"pt": "Sudão",
		"en": "Sudan",
		"fullname": "Sudan",
		"continent": "af",
		"es": "Sudán"
	},
	"SLB": {
		"alpha2": "SB",
		"code": "ocslb",
		"pt": "Ilhas Salomão",
		"en": "Solomon Islands",
		"fullname": "Solomon Islands",
		"continent": "oc",
		"es": "Islas Salomón"
	},
	"MCO": {
		"alpha2": "MC",
		"code": "eumco",
		"pt": "Mônaco",
		"en": "Monaco",
		"fullname": "Monaco",
		"continent": "eu",
		"es": "Mónaco"
	},
	"BRB": {
		"alpha2": "BB",
		"code": "nabrb",
		"pt": "Barbados",
		"en": "Barbados",
		"fullname": "Barbados",
		"continent": "na",
		"es": "Barbados"
	},
	"KGZ": {
		"alpha2": "KG",
		"code": "askgz",
		"pt": "Kirgizstán",
		"en": "Kyrgyzstan",
		"fullname": "Kyrgyzstan",
		"continent": "as",
		"es": "Kirgizstán"
	},
	"TKL": {
		"alpha2": "TK",
		"code": "octkl",
		"pt": "Tokelau",
		"en": "Tokelau",
		"fullname": "Tokelau",
		"continent": "oc",
		"es": "Tokelau"
	},
	"NRU": {
		"alpha2": "NR",
		"code": "ocnru",
		"pt": "Nauru",
		"en": "Nauru",
		"fullname": "Nauru",
		"continent": "oc",
		"es": "Nauru"
	}
}
