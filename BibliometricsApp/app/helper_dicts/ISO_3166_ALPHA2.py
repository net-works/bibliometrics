#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA2 = {
	"WF": {
		"alpha3": "WLF",
		"code": "ocwlf",
		"en": "Wallis and Futuna",
		"pt": "Wallis y Futuna",
		"fullname": "Wallis and Futuna Islands",
		"demonym": "Wallisian",
		"continent": "oc",
		"es": "Wallis y Futuna"
	},
	"JP": {
		"alpha3": "JPN",
		"code": "asjpn",
		"en": "Japan",
		"pt": "Japão",
		"fullname": "Japan",
		"demonym": "Japanese",
		"continent": "as",
		"es": "Japón"
	},
	"JM": {
		"alpha3": "JAM",
		"code": "najam",
		"en": "Jamaica",
		"pt": "Jamaica",
		"fullname": "Jamaica",
		"demonym": "Jamaican",
		"continent": "na",
		"es": "Jamaica"
	},
	"JO": {
		"alpha3": "JOR",
		"code": "asjor",
		"en": "Jordan",
		"pt": "Jordânia",
		"fullname": "Jordan",
		"demonym": "Jordanian",
		"continent": "as",
		"es": "Jordania"
	},
	"WS": {
		"alpha3": "WSM",
		"code": "ocwsm",
		"en": "Samoa",
		"pt": "Samoa",
		"fullname": "Samoa",
		"demonym": "Samoan",
		"continent": "oc",
		"es": "Samoa"
	},
	"GW": {
		"alpha3": "GNB",
		"code": "afgnb",
		"en": "Guinea-Bissau",
		"pt": "Guiné-Bissau",
		"fullname": "Guinea-Bissau",
		"demonym": "Guinean",
		"continent": "af",
		"es": "Guinea-Bissau"
	},
	"GU": {
		"alpha3": "GUM",
		"code": "ocgum",
		"en": "Guam",
		"pt": "Guam",
		"fullname": "Guam",
		"demonym": "Guamanian",
		"continent": "oc",
		"es": "Guam"
	},
	"GT": {
		"alpha3": "GTM",
		"code": "nagtm",
		"en": "Guatemala",
		"pt": "Guatemala",
		"fullname": "Guatemala",
		"demonym": "Guatemalan",
		"continent": "na",
		"es": "Guatemala"
	},
	"GR": {
		"alpha3": "GRC",
		"code": "eugrc",
		"en": "Greece",
		"pt": "Grécia",
		"fullname": "Greece",
		"demonym": "Greek",
		"continent": "eu",
		"es": "Grecia"
	},
	"GQ": {
		"alpha3": "GNQ",
		"code": "afgnq",
		"en": "Equatorial Guinea",
		"pt": "Guiné-Equatorial",
		"fullname": "Equatorial Guinea",
		"demonym": "Equatorial Guinean",
		"continent": "af",
		"es": "Guinea Ecuatorial"
	},
	"GP": {
		"alpha3": "GLP",
		"code": "naglp",
		"en": "Guadeloupe",
		"pt": "Guadalupe",
		"fullname": "Guadeloupe",
		"demonym": "Guadeloupean",
		"continent": "na",
		"es": "Guadalupe"
	},
	"GY": {
		"alpha3": "GUY",
		"code": "saguy",
		"en": "Guyana",
		"pt": "Guiana",
		"fullname": "Guyana",
		"demonym": "Guyanese",
		"continent": "sa",
		"es": "Guyana"
	},
	"GF": {
		"alpha3": "GUF",
		"code": "saguf",
		"en": "French Guiana",
		"pt": "Guayana Francesa",
		"fullname": "French Guiana",
		"demonym": "French Guianese",
		"continent": "sa",
		"es": "Guayana Francesa"
	},
	"GE": {
		"alpha3": "GEO",
		"code": "asgeo",
		"en": "Georgia",
		"pt": "Geórgia",
		"fullname": "Georgia",
		"demonym": "Georgian",
		"continent": "as",
		"es": "Georgia"
	},
	"GD": {
		"alpha3": "GRD",
		"code": "nagrd",
		"en": "Grenada",
		"pt": "Granada",
		"fullname": "Grenada and Carriacuou",
		"demonym": "Grenadian",
		"continent": "na",
		"es": "Granada"
	},
	"GB": {
		"alpha3": "GBR",
		"code": "eugbr",
		"en": "United Kingdom",
		"pt": "Reino Unido",
		"fullname": "United Kingdom",
		"demonym": "British",
		"continent": "eu",
		"es": "Reino Unido"
	},
	"GA": {
		"alpha3": "GAB",
		"code": "afgab",
		"en": "Gabon",
		"pt": "Gabão",
		"fullname": "Gabon Republic",
		"demonym": "Gabonese",
		"continent": "af",
		"es": "Gabón"
	},
	"GN": {
		"alpha3": "GIN",
		"code": "afgin",
		"en": "Guinea",
		"pt": "Guiné",
		"fullname": "Guinea",
		"demonym": "Guinean",
		"continent": "af",
		"es": "Guinea"
	},
	"GM": {
		"alpha3": "GMB",
		"code": "afgmb",
		"en": "Gambia",
		"pt": "Gambia",
		"fullname": "Gambia",
		"demonym": "Gambian",
		"continent": "af",
		"es": "Gambia"
	},
	"GL": {
		"alpha3": "GRL",
		"code": "nagrl",
		"en": "Greenland",
		"pt": "Groenlândia",
		"fullname": "Greenland",
		"demonym": "Greenlander",
		"continent": "na",
		"es": "Groenlandia"
	},
	"GI": {
		"alpha3": "GIB",
		"code": "eugib",
		"en": "Gibraltar",
		"pt": "Gibraltar",
		"fullname": "Gibraltar",
		"demonym": "Gibralterian",
		"continent": "eu",
		"es": "Gibraltar"
	},
	"GH": {
		"alpha3": "GHA",
		"code": "afgha",
		"en": "Ghana",
		"pt": "Gana",
		"fullname": "Ghana",
		"demonym": "Ghanaian",
		"continent": "af",
		"es": "Ghana"
	},
	"PR": {
		"alpha3": "PRI",
		"code": "napri",
		"en": "Puerto Rico",
		"pt": "Porto Rico",
		"fullname": "Puerto Rico",
		"demonym": "Puerto Rican",
		"continent": "na",
		"es": "Puerto Rico"
	},
	"PS": {
		"alpha3": "PSE",
		"code": "aspse",
		"en": "Palestine",
		"pt": "Palestina",
		"fullname": "Palestine",
		"demonym": "Palestinian",
		"continent": "as",
		"es": "Palestina"
	},
	"PW": {
		"alpha3": "PLW",
		"code": "ocplw",
		"en": "Palau",
		"pt": "Palau",
		"fullname": "Palau",
		"demonym": "Palauan",
		"continent": "oc",
		"es": "Palau"
	},
	"PT": {
		"alpha3": "PRT",
		"code": "euprt",
		"en": "Portugal",
		"pt": "Portugal",
		"fullname": "Portugal",
		"demonym": "Portuguese",
		"continent": "eu",
		"es": "Portugal"
	},
	"PY": {
		"alpha3": "PRY",
		"code": "sapry",
		"en": "Paraguay",
		"pt": "Paraguai",
		"fullname": "Paraguay",
		"demonym": "Paraguayan",
		"continent": "sa",
		"es": "Paraguay"
	},
	"PA": {
		"alpha3": "PAN",
		"code": "napan",
		"en": "Panama",
		"pt": "Panamá",
		"fullname": "Panama",
		"demonym": "Panamanian",
		"continent": "na",
		"es": "Panamá"
	},
	"PF": {
		"alpha3": "PYF",
		"code": "ocpyf",
		"en": "French Polynesia",
		"pt": "Polinésia Francesa",
		"fullname": "French Polynesia",
		"demonym": "French Polynesian",
		"continent": "oc",
		"es": "Polinesia Francesa"
	},
	"PG": {
		"alpha3": "PNG",
		"code": "ocpng",
		"en": "Papua New Guinea",
		"pt": "Papua-Nova-Guiné",
		"fullname": "Papua New Guinea",
		"demonym": "Papua New Guinean",
		"continent": "oc",
		"es": "Papúa Nueva Guinea"
	},
	"PE": {
		"alpha3": "PER",
		"code": "saper",
		"en": "Peru",
		"pt": "Peru",
		"fullname": "Peru",
		"demonym": "Peruvian",
		"continent": "sa",
		"es": "Perú"
	},
	"PK": {
		"alpha3": "PAK",
		"code": "aspak",
		"en": "Pakistan",
		"pt": "Paquistão",
		"fullname": "Pakistan",
		"demonym": "Pakistani",
		"continent": "as",
		"es": "Pakistán"
	},
	"PH": {
		"alpha3": "PHL",
		"code": "asphl",
		"en": "Philippines",
		"pt": "Filipinas",
		"fullname": "Philippines",
		"demonym": "Filipino",
		"continent": "as",
		"es": "Filipinas"
	},
	"PL": {
		"alpha3": "POL",
		"code": "eupol",
		"en": "Poland",
		"pt": "Polônia",
		"fullname": "Poland",
		"demonym": "Polish",
		"continent": "eu",
		"es": "Polonia"
	},
	"PM": {
		"alpha3": "SPM",
		"code": "naspm",
		"en": "Saint Pierre and Miquelon",
		"pt": "San Pedro y Miquelón",
		"fullname": "St. Pierre and Miquelon",
		"demonym": "Saint-Pierrais",
		"continent": "na",
		"es": "San Pedro y Miquelón"
	},
	"ZM": {
		"alpha3": "ZMB",
		"code": "afzmb",
		"en": "Zambia",
		"pt": "Zâmbia",
		"fullname": "Zambia",
		"demonym": "Zambian",
		"continent": "af",
		"es": "Zambia"
	},
	"ZA": {
		"alpha3": "ZAF",
		"code": "afzaf",
		"en": "South Africa",
		"pt": "África do Sul",
		"fullname": "South Africa",
		"demonym": "South African",
		"continent": "af",
		"es": "Sudáfrica"
	},
	"ZW": {
		"alpha3": "ZWE",
		"code": "afzwe",
		"en": "Zimbabwe",
		"pt": "Zimbábue",
		"fullname": "Zimbabwe",
		"demonym": "Zimbabwean",
		"continent": "af",
		"es": "Zimbabue"
	},
	"ZR": {
		"alpha3": "COD",
		"fullname": "Dem. Republic of the Congo",
		"code": "afcod",
		"demonym": "Congolese",
		"continent": "af"
	},
	"ME": {
		"alpha3": "MNE",
		"code": "eumne",
		"en": "Montenegro",
		"pt": "Montenegro",
		"fullname": "Montenegro",
		"demonym": "Montenegrin",
		"continent": "eu",
		"es": "Montenegro"
	},
	"MD": {
		"alpha3": "MDA",
		"code": "eumda",
		"en": "Moldova",
		"pt": "Moldavia",
		"fullname": "Moldova",
		"demonym": "Moldovan",
		"continent": "eu",
		"es": "Moldavia"
	},
	"MG": {
		"alpha3": "MDG",
		"code": "afmdg",
		"en": "Madagascar",
		"pt": "Madagascar",
		"fullname": "Madagascar",
		"demonym": "Malagasy",
		"continent": "af",
		"es": "Madagascar"
	},
	"MA": {
		"alpha3": "MAR",
		"code": "afmar",
		"en": "Morocco",
		"pt": "Marrocos",
		"fullname": "Morocco",
		"demonym": "Moroccan",
		"continent": "af",
		"es": "Marruecos"
	},
	"MC": {
		"alpha3": "MCO",
		"code": "eumco",
		"en": "Monaco",
		"pt": "Mônaco",
		"fullname": "Monaco",
		"demonym": "Monacan",
		"continent": "eu",
		"es": "Mónaco"
	},
	"MM": {
		"alpha3": "MMR",
		"code": "asmmr",
		"en": "Myanmar",
		"pt": "Birmania",
		"fullname": "Myanmar",
		"demonym": "Myanmarese",
		"continent": "as",
		"es": "Birmania"
	},
	"ML": {
		"alpha3": "MLI",
		"code": "afmli",
		"en": "Mali",
		"pt": "Mali",
		"fullname": "Mali Republic",
		"demonym": "Malian",
		"continent": "af",
		"es": "Mali"
	},
	"MO": {
		"alpha3": "MAC",
		"code": "asmac",
		"en": "Macao",
		"pt": "Macao",
		"fullname": "Macau",
		"demonym": "Macanese",
		"continent": "as",
		"es": "Macao"
	},
	"MN": {
		"alpha3": "MNG",
		"code": "asmng",
		"en": "Mongolia",
		"pt": "Mongólia",
		"fullname": "Mongolia",
		"demonym": "Mongolian",
		"continent": "as",
		"es": "Mongolia"
	},
	"MH": {
		"alpha3": "MHL",
		"code": "ocmhl",
		"en": "Marshall Islands",
		"pt": "Islas Marshall",
		"fullname": "Marshall Islands",
		"demonym": "Marshallese",
		"continent": "oc",
		"es": "Islas Marshall"
	},
	"MK": {
		"alpha3": "MKD",
		"code": "eumkd",
		"en": "Macedonia",
		"pt": "Macedonia",
		"fullname": "Macedonia",
		"demonym": "Macedonian",
		"continent": "eu",
		"es": "Macedonia"
	},
	"MU": {
		"alpha3": "MUS",
		"code": "afmus",
		"en": "Mauritius",
		"pt": "Mauricio",
		"fullname": "Mauritius",
		"demonym": "Mauritian",
		"continent": "af",
		"es": "Mauricio"
	},
	"MT": {
		"alpha3": "MLT",
		"code": "eumlt",
		"en": "Malta",
		"pt": "Malta",
		"fullname": "Malta",
		"demonym": "Maltese",
		"continent": "eu",
		"es": "Malta"
	},
	"MW": {
		"alpha3": "MWI",
		"code": "afmwi",
		"en": "Malawi",
		"pt": "Malawi",
		"fullname": "Malawi",
		"demonym": "Malawian",
		"continent": "af",
		"es": "Malawi"
	},
	"MV": {
		"alpha3": "MDV",
		"code": "asmdv",
		"en": "Maldives",
		"pt": "Islas Maldivas",
		"fullname": "Maldives",
		"demonym": "Maldivan",
		"continent": "as",
		"es": "Islas Maldivas"
	},
	"MQ": {
		"alpha3": "MTQ",
		"code": "namtq",
		"en": "Martinique",
		"pt": "Martinica",
		"fullname": "Martinique",
		"demonym": "Martinican",
		"continent": "na",
		"es": "Martinica"
	},
	"MP": {
		"alpha3": "MNP",
		"code": "ocmnp",
		"en": "Northern Mariana Islands",
		"pt": "Islas Marianas del Norte",
		"fullname": "Saipan",
		"demonym": "Northern Mariana Islander",
		"continent": "oc",
		"es": "Islas Marianas del Norte"
	},
	"MS": {
		"alpha3": "MSR",
		"code": "namsr",
		"en": "Montserrat",
		"pt": "Montserrat",
		"fullname": "Montserrat",
		"demonym": "Montserratian",
		"continent": "na",
		"es": "Montserrat"
	},
	"MR": {
		"alpha3": "MRT",
		"code": "afmrt",
		"en": "Mauritania",
		"pt": "Mauritânia",
		"fullname": "Mauritania",
		"demonym": "Mauritanian",
		"continent": "af",
		"es": "Mauritania"
	},
	"MY": {
		"alpha3": "MYS",
		"code": "asmys",
		"en": "Malaysia",
		"pt": "Malásia",
		"fullname": "Malaysia",
		"demonym": "Malaysian",
		"continent": "as",
		"es": "Malasia"
	},
	"MX": {
		"alpha3": "MEX",
		"code": "namex",
		"en": "Mexico",
		"pt": "México",
		"fullname": "Mexico",
		"demonym": "Mexican",
		"continent": "na",
		"es": "México"
	},
	"MZ": {
		"alpha3": "MOZ",
		"code": "afmoz",
		"en": "Mozambique",
		"pt": "Moçambique",
		"fullname": "Mozambique",
		"demonym": "Mozambican",
		"continent": "af",
		"es": "Mozambique"
	},
	"FR": {
		"alpha3": "FRA",
		"code": "eufra",
		"en": "France",
		"pt": "França",
		"fullname": "France",
		"demonym": "French",
		"continent": "eu",
		"es": "Francia"
	},
	"FI": {
		"alpha3": "FIN",
		"code": "eufin",
		"en": "Finland",
		"pt": "Finlândia",
		"fullname": "Finland",
		"demonym": "Finnish",
		"continent": "eu",
		"es": "Finlandia"
	},
	"FJ": {
		"alpha3": "FJI",
		"code": "ocfji",
		"en": "Fiji",
		"pt": "Fiji",
		"fullname": "Fiji Islands",
		"demonym": "Fijian",
		"continent": "oc",
		"es": "Fiyi"
	},
	"FK": {
		"alpha3": "FLK",
		"code": "saflk",
		"en": "Falkland Islands (Malvinas)",
		"pt": "Islas Malvinas",
		"fullname": "Falkland Islands",
		"demonym": "Falkland Islander",
		"continent": "sa",
		"es": "Islas Malvinas"
	},
	"FM": {
		"alpha3": "FSM",
		"code": "ocfsm",
		"en": "Estados Federados de",
		"pt": "Micronesia",
		"fullname": "Federated States of Micronesia",
		"demonym": "Micronesian",
		"continent": "oc",
		"es": "Micronesia"
	},
	"FO": {
		"alpha3": "FRO",
		"code": "eufro",
		"en": "Faroe Islands",
		"pt": "Islas Feroe",
		"fullname": "Faroe Islands",
		"demonym": "Faroese",
		"continent": "eu",
		"es": "Islas Feroe"
	},
	"CK": {
		"alpha3": "COK",
		"code": "occok",
		"en": "Cook Islands",
		"pt": "Ilhas Cook",
		"fullname": "Cook Islands",
		"demonym": "Cook Islander",
		"continent": "oc",
		"es": "Islas Cook"
	},
	"CI": {
		"alpha3": "CIV",
		"code": "afciv",
		"en": "Ivory Coast",
		"pt": "Costa do Marfim",
		"fullname": "Ivory Coast",
		"demonym": "Ivorian",
		"continent": "af",
		"es": "Costa de Marfil"
	},
	"CH": {
		"alpha3": "CHE",
		"code": "euche",
		"en": "Switzerland",
		"pt": "Suíça",
		"fullname": "Switzerland",
		"demonym": "Swiss",
		"continent": "eu",
		"es": "Suiza"
	},
	"CO": {
		"alpha3": "COL",
		"code": "sacol",
		"en": "Colombia",
		"pt": "Colombia",
		"fullname": "Colombia",
		"demonym": "Colombian",
		"continent": "sa",
		"es": "Colombia"
	},
	"CN": {
		"alpha3": "CHN",
		"code": "aschn",
		"en": "China",
		"pt": "China",
		"fullname": "China",
		"demonym": "Chinese",
		"continent": "as",
		"es": "China"
	},
	"CM": {
		"alpha3": "CMR",
		"code": "afcmr",
		"en": "Cameroon",
		"pt": "Camarões",
		"fullname": "Cameroon",
		"demonym": "Cameroonian",
		"continent": "af",
		"es": "Camerún"
	},
	"CL": {
		"alpha3": "CHL",
		"code": "sachl",
		"en": "Chile",
		"pt": "Chile",
		"fullname": "Chile",
		"demonym": "Chilean",
		"continent": "sa",
		"es": "Chile"
	},
	"CC": {
		"alpha3": "CCK",
		"code": "ascck",
		"en": "Cocos (Keeling) Islands",
		"pt": "Islas Cocos (Keeling)",
		"fullname": "Cocos Islands",
		"demonym": "Cocossian",
		"continent": "as",
		"es": "Islas Cocos (Keeling)"
	},
	"CA": {
		"alpha3": "CAN",
		"code": "nacan",
		"en": "Canada",
		"pt": "Canadá",
		"fullname": "Canada",
		"demonym": "Canadian",
		"continent": "na",
		"es": "Canadá"
	},
	"CG": {
		"alpha3": "COG",
		"code": "afcog",
		"en": "Congo",
		"pt": "Congo",
		"fullname": "Congo",
		"demonym": "Congolese",
		"continent": "af",
		"es": "Congo"
	},
	"CF": {
		"alpha3": "CAF",
		"code": "afcaf",
		"en": "Central African Republic",
		"pt": "República Centro-Africana",
		"fullname": "Central African Republic",
		"demonym": "Central African",
		"continent": "af",
		"es": "República Centroafricana"
	},
	"CD": {
		"alpha3": "COD",
		"code": "afcod",
		"en": "Congo",
		"pt": "Congo",
		"fullname": "Dem. Republic of the Congo",
		"demonym": "Congolese",
		"continent": "af",
		"es": "Congo"
	},
	"CZ": {
		"alpha3": "CZE",
		"code": "eucze",
		"en": "Czech Republic",
		"pt": "República Tcheca",
		"fullname": "Czech Republic",
		"demonym": "Czech",
		"continent": "eu",
		"es": "República Checa"
	},
	"CY": {
		"alpha3": "CYP",
		"code": "eucyp",
		"en": "Cyprus",
		"pt": "Chipre",
		"fullname": "Cyprus",
		"demonym": "Cypriot",
		"continent": "eu",
		"es": "Chipre"
	},
	"CX": {
		"alpha3": "CXR",
		"code": "ascxr",
		"en": "Christmas Island",
		"pt": "Isla de Navidad",
		"fullname": "Christmas Island",
		"demonym": "Christmas Islander",
		"continent": "as",
		"es": "Isla de Navidad"
	},
	"CR": {
		"alpha3": "CRI",
		"code": "nacri",
		"en": "Costa Rica",
		"pt": "Costa Rica",
		"fullname": "Costa Rica",
		"demonym": "Costa Rican",
		"continent": "na",
		"es": "Costa Rica"
	},
	"CW": {
		"alpha3": "ANT",
		"fullname": "Netherlands Antilles",
		"code": "naant",
		"demonym": "Cura\\u00e7aoan",
		"continent": "na"
	},
	"CV": {
		"alpha3": "CPV",
		"code": "afcpv",
		"en": "Cape Verde",
		"pt": "Cabo Verde",
		"fullname": "Cape Verde Islands",
		"demonym": "Cape Verdean",
		"continent": "af",
		"es": "Cabo Verde"
	},
	"CU": {
		"alpha3": "CUB",
		"code": "nacub",
		"en": "Cuba",
		"pt": "Cuba",
		"fullname": "Cuba",
		"demonym": "Cuban",
		"continent": "na",
		"es": "Cuba"
	},
	"SZ": {
		"alpha3": "SWZ",
		"code": "afswz",
		"en": "Swaziland",
		"pt": "Suazilândia",
		"fullname": "Swaziland",
		"demonym": "Swazi",
		"continent": "af",
		"es": "Swazilandia"
	},
	"SY": {
		"alpha3": "SYR",
		"code": "assyr",
		"en": "Syria",
		"pt": "Síria",
		"fullname": "Syria",
		"demonym": "Syrian",
		"continent": "as",
		"es": "Siria"
	},
	"SX": {
		"alpha3": "SXM",
		"fullname": "Sint Maarten",
		"code": "nasxm",
		"demonym": "Saint Martin (Netherlands)",
		"continent": "na"
	},
	"SS": {
		"alpha3": "SSD",
		"fullname": "South Sudan",
		"code": "afssd",
		"demonym": "Sudanese",
		"continent": "af"
	},
	"SR": {
		"alpha3": "SUR",
		"code": "sasur",
		"en": "Suriname",
		"pt": "Surinám",
		"fullname": "Suriname",
		"demonym": "Surinamer",
		"continent": "sa",
		"es": "Surinám"
	},
	"SV": {
		"alpha3": "SLV",
		"code": "naslv",
		"en": "El Salvador",
		"pt": "El Salvador",
		"fullname": "El Salvador",
		"demonym": "Salvadorean",
		"continent": "na",
		"es": "El Salvador"
	},
	"ST": {
		"alpha3": "STP",
		"code": "afstp",
		"en": "Sao Tome and Principe",
		"pt": "Santo Tomé y Príncipe",
		"fullname": "Sao Tome",
		"demonym": "S\\u00e3o Tomean",
		"continent": "af",
		"es": "Santo Tomé y Príncipe"
	},
	"SK": {
		"alpha3": "SVK",
		"code": "eusvk",
		"en": "Slovakia",
		"pt": "Eslováquia",
		"fullname": "Slovakia",
		"demonym": "Slovakian",
		"continent": "eu",
		"es": "Eslovaquia"
	},
	"SI": {
		"alpha3": "SVN",
		"code": "eusvn",
		"en": "Slovenia",
		"pt": "Eslovênia",
		"fullname": "Slovenia",
		"demonym": "Slovenian",
		"continent": "eu",
		"es": "Eslovenia"
	},
	"SH": {
		"alpha3": "SHN",
		"code": "afshn",
		"en": "Ascensión y Tristán de Acuña",
		"pt": "Santa Elena",
		"fullname": "St. Helena",
		"demonym": "Saint Helenian",
		"continent": "af",
		"es": "Santa Elena"
	},
	"SO": {
		"alpha3": "SOM",
		"code": "afsom",
		"en": "Somalia",
		"pt": "Somália",
		"fullname": "Somalia Republic",
		"demonym": "Somali",
		"continent": "af",
		"es": "Somalia"
	},
	"SN": {
		"alpha3": "SEN",
		"code": "afsen",
		"en": "Senegal",
		"pt": "Senegal",
		"fullname": "Senegal Republic",
		"demonym": "Senegalese",
		"continent": "af",
		"es": "Senegal"
	},
	"SM": {
		"alpha3": "SMR",
		"code": "eusmr",
		"en": "San Marino",
		"pt": "San Marino",
		"fullname": "San Marino",
		"demonym": "Sanmarinese",
		"continent": "eu",
		"es": "San Marino"
	},
	"SL": {
		"alpha3": "SLE",
		"code": "afsle",
		"en": "Sierra Leone",
		"pt": "Sierra Leona",
		"fullname": "Sierra Leone",
		"demonym": "Sierra Leonean",
		"continent": "af",
		"es": "Sierra Leona"
	},
	"SC": {
		"alpha3": "SYC",
		"code": "afsyc",
		"en": "Seychelles",
		"pt": "Seychelles",
		"fullname": "Seychelles",
		"demonym": "Seychellois",
		"continent": "af",
		"es": "Seychelles"
	},
	"SB": {
		"alpha3": "SLB",
		"code": "ocslb",
		"en": "Solomon Islands",
		"pt": "Ilhas Salomão",
		"fullname": "Solomon Islands",
		"demonym": "Solomon Islander",
		"continent": "oc",
		"es": "Islas Salomón"
	},
	"SA": {
		"alpha3": "SAU",
		"code": "assau",
		"en": "Saudi Arabia",
		"pt": "Arábia Saudita",
		"fullname": "Saudi Arabia",
		"demonym": "Saudi Arabian",
		"continent": "as",
		"es": "Arabia Saudita"
	},
	"SG": {
		"alpha3": "SGP",
		"code": "assgp",
		"en": "Singapore",
		"pt": "Cingapura",
		"fullname": "Singapore",
		"demonym": "Singaporean",
		"continent": "as",
		"es": "Singapur"
	},
	"SE": {
		"alpha3": "SWE",
		"code": "euswe",
		"en": "Sweden",
		"pt": "Suécia",
		"fullname": "Sint Eustatius",
		"demonym": "Swedish",
		"continent": "na",
		"es": "Suecia"
	},
	"SD": {
		"alpha3": "SDN",
		"code": "afsdn",
		"en": "Sudan",
		"pt": "Sudão",
		"fullname": "Sudan",
		"demonym": "Sudanese",
		"continent": "af",
		"es": "Sudán"
	},
	"YE": {
		"alpha3": "YEM",
		"code": "asyem",
		"en": "Yemen",
		"pt": "Iêmen",
		"fullname": "Yemen",
		"demonym": "Yemeni",
		"continent": "as",
		"es": "Yemen"
	},
	"YT": {
		"alpha3": "MYT",
		"code": "afmyt",
		"en": "Mayotte",
		"pt": "Mayotte",
		"fullname": "Mayotte Island",
		"demonym": "Mahoran",
		"continent": "af",
		"es": "Mayotte"
	},
	"LB": {
		"alpha3": "LBN",
		"code": "aslbn",
		"en": "Lebanon",
		"pt": "Líbano",
		"fullname": "Lebanon",
		"demonym": "Lebanese",
		"continent": "as",
		"es": "Líbano"
	},
	"LC": {
		"alpha3": "LCA",
		"code": "nalca",
		"en": "Saint Lucia",
		"pt": "Santa Lucía",
		"fullname": "St. Lucia",
		"demonym": "Saint Lucian",
		"continent": "na",
		"es": "Santa Lucía"
	},
	"LA": {
		"alpha3": "LAO",
		"code": "aslao",
		"en": "Laos",
		"pt": "Laos",
		"fullname": "Laos",
		"demonym": "Laotian",
		"continent": "as",
		"es": "Laos"
	},
	"LK": {
		"alpha3": "LKA",
		"code": "aslka",
		"en": "Sri Lanka",
		"pt": "Sri Lanka",
		"fullname": "Sri Lanka",
		"demonym": "Sri Lankan",
		"continent": "as",
		"es": "Sri lanka"
	},
	"LI": {
		"alpha3": "LIE",
		"code": "eulie",
		"en": "Liechtenstein",
		"pt": "Liechtenstein",
		"fullname": "Liechtenstein",
		"demonym": "Liechtensteiner",
		"continent": "eu",
		"es": "Liechtenstein"
	},
	"LV": {
		"alpha3": "LVA",
		"code": "eulva",
		"en": "Latvia",
		"pt": "Letonia",
		"fullname": "Latvia",
		"demonym": "Latvian",
		"continent": "eu",
		"es": "Letonia"
	},
	"LT": {
		"alpha3": "LTU",
		"code": "eultu",
		"en": "Lithuania",
		"pt": "Lituânia",
		"fullname": "Lithuania",
		"demonym": "Lithunian",
		"continent": "eu",
		"es": "Lituania"
	},
	"LU": {
		"alpha3": "LUX",
		"code": "eulux",
		"en": "Luxembourg",
		"pt": "Luxemburgo",
		"fullname": "Luxembourg",
		"demonym": "Luxembourger",
		"continent": "eu",
		"es": "Luxemburgo"
	},
	"LR": {
		"alpha3": "LBR",
		"code": "aflbr",
		"en": "Liberia",
		"pt": "Libéria",
		"fullname": "Liberia",
		"demonym": "Liberian",
		"continent": "af",
		"es": "Liberia"
	},
	"LS": {
		"alpha3": "LSO",
		"code": "aflso",
		"en": "Lesotho",
		"pt": "Lesoto",
		"fullname": "Lesotho",
		"demonym": "Mosotho",
		"continent": "af",
		"es": "Lesoto"
	},
	"LY": {
		"alpha3": "LBY",
		"code": "aflby",
		"en": "Libya",
		"pt": "Libia",
		"fullname": "Libya",
		"demonym": "Libyan",
		"continent": "af",
		"es": "Libia"
	},
	"VA": {
		"alpha3": "VAT",
		"code": "euvat",
		"en": "Vatican City State",
		"pt": "Ciudad del Vaticano",
		"fullname": "Vatican city",
		"demonym": "Vatican",
		"continent": "eu",
		"es": "Ciudad del Vaticano"
	},
	"VC": {
		"alpha3": "VCT",
		"code": "navct",
		"en": "Saint Vincent and the Grenadines",
		"pt": "San Vicente y las Granadinas",
		"fullname": "St. Vincent",
		"demonym": "Saint Vincentian",
		"continent": "na",
		"es": "San Vicente y las Granadinas"
	},
	"VE": {
		"alpha3": "VEN",
		"code": "saven",
		"en": "Venezuela",
		"pt": "Venezuela",
		"fullname": "Venezuela",
		"demonym": "Venezuelan",
		"continent": "sa",
		"es": "Venezuela"
	},
	"VG": {
		"alpha3": "VGB",
		"code": "navgb",
		"en": "Virgin Islands",
		"pt": "Ilhas Virgens",
		"fullname": "British Virgin Islands",
		"demonym": "Virgin Islander",
		"continent": "na",
		"es": "Islas Vírgenes Británicas"
	},
	"IQ": {
		"alpha3": "IRQ",
		"code": "asirq",
		"en": "Iraq",
		"pt": "Iraque",
		"fullname": "Iraq",
		"demonym": "Iraqi",
		"continent": "as",
		"es": "Irak"
	},
	"VI": {
		"alpha3": "VIR",
		"code": "navir",
		"en": "United States Virgin Islands",
		"pt": "Islas Vírgenes de los Estados Unidos",
		"fullname": "US Virgin Islands",
		"demonym": "Virgin Islander",
		"continent": "na",
		"es": "Islas Vírgenes de los Estados Unidos"
	},
	"IS": {
		"alpha3": "ISL",
		"code": "euisl",
		"en": "Iceland",
		"pt": "Islândia",
		"fullname": "Iceland",
		"demonym": "Iceland",
		"continent": "eu",
		"es": "Islandia"
	},
	"IR": {
		"alpha3": "IRN",
		"code": "asirn",
		"en": "Iran",
		"pt": "Irã",
		"fullname": "Iran",
		"demonym": "Iranian",
		"continent": "as",
		"es": "Irán"
	},
	"IT": {
		"alpha3": "ITA",
		"code": "euita",
		"en": "Italy",
		"pt": "Itália",
		"fullname": "Italy",
		"demonym": "Italian",
		"continent": "eu",
		"es": "Italia"
	},
	"VN": {
		"alpha3": "VNM",
		"code": "asvnm",
		"en": "Vietnam",
		"pt": "Vietnã",
		"fullname": "Vietnam",
		"demonym": "Vietnamese",
		"continent": "as",
		"es": "Vietnam"
	},
	"IM": {
		"alpha3": "IMN",
		"code": "euimn",
		"en": "Isle of Man",
		"pt": "Ilha de Man",
		"fullname": "Isle of Man",
		"demonym": "Manx",
		"continent": "eu",
		"es": "Isla de Man"
	},
	"IL": {
		"alpha3": "ISR",
		"code": "asisr",
		"en": "Israel",
		"pt": "Israel",
		"fullname": "Israel",
		"demonym": "Israeli",
		"continent": "as",
		"es": "Israel"
	},
	"IO": {
		"alpha3": "IOT",
		"code": "asiot",
		"en": "British Indian Ocean Territory",
		"pt": "Territorio Británico del Océano Índico",
		"fullname": "Diego Garcia",
		"demonym": "British Indian Ocean Territory",
		"continent": "as",
		"es": "Territorio Británico del Océano Índico"
	},
	"IN": {
		"alpha3": "IND",
		"code": "asind",
		"en": "India",
		"pt": "Índia",
		"fullname": "India",
		"demonym": "Indian",
		"continent": "as",
		"es": "India"
	},
	"IE": {
		"alpha3": "IRL",
		"code": "euirl",
		"en": "Ireland",
		"pt": "Irlanda",
		"fullname": "Ireland",
		"demonym": "Irish",
		"continent": "eu",
		"es": "Irlanda"
	},
	"ID": {
		"alpha3": "IDN",
		"code": "asidn",
		"en": "Indonesia",
		"pt": "Indonésia",
		"fullname": "Indonesia",
		"demonym": "Indonesian",
		"continent": "as",
		"es": "Indonesia"
	},
	"BD": {
		"alpha3": "BGD",
		"code": "asbgd",
		"en": "Bangladesh",
		"pt": "Bangladesh",
		"fullname": "Bangladesh",
		"demonym": "Bangladeshi",
		"continent": "as",
		"es": "Bangladesh"
	},
	"BE": {
		"alpha3": "BEL",
		"code": "eubel",
		"en": "Belgium",
		"pt": "Bélgica",
		"fullname": "Belgium",
		"demonym": "Belgian",
		"continent": "eu",
		"es": "Bélgica"
	},
	"BF": {
		"alpha3": "BFA",
		"code": "afbfa",
		"en": "Burkina Faso",
		"pt": "Burquina Faso",
		"fullname": "Burkina Faso",
		"demonym": "Burkinabe",
		"continent": "af",
		"es": "Burkina Faso"
	},
	"BG": {
		"alpha3": "BGR",
		"code": "eubgr",
		"en": "Bulgaria",
		"pt": "Bulgária",
		"fullname": "Bulgaria",
		"demonym": "Bulgarian",
		"continent": "eu",
		"es": "Bulgaria"
	},
	"BA": {
		"alpha3": "BIH",
		"code": "eubih",
		"en": "Bosnia and Herzegovina",
		"pt": "Bosnia y Herzegovina",
		"fullname": "Bosnia and Herzegovina",
		"demonym": "Bosnian",
		"continent": "eu",
		"es": "Bosnia y Herzegovina"
	},
	"BB": {
		"alpha3": "BRB",
		"code": "nabrb",
		"en": "Barbados",
		"pt": "Barbados",
		"fullname": "Barbados",
		"demonym": "Barbadian",
		"continent": "na",
		"es": "Barbados"
	},
	"BM": {
		"alpha3": "BMU",
		"code": "nabmu",
		"en": "Bermuda Islands",
		"pt": "Islas Bermudas",
		"fullname": "Bermuda",
		"demonym": "Bermudan",
		"continent": "na",
		"es": "Islas Bermudas"
	},
	"BN": {
		"alpha3": "BRN",
		"code": "asbrn",
		"en": "Brunei",
		"pt": "Brunei",
		"fullname": "Brunei",
		"demonym": "Bruneian",
		"continent": "as",
		"es": "Brunéi"
	},
	"BO": {
		"alpha3": "BOL",
		"code": "sabol",
		"en": "Bolivia",
		"pt": "Bolívia",
		"fullname": "Bolivia",
		"demonym": "Bolivian",
		"continent": "sa",
		"es": "Bolivia"
	},
	"BH": {
		"alpha3": "BHR",
		"code": "asbhr",
		"en": "Bahrain",
		"pt": "Bahrein",
		"fullname": "Bahrain",
		"demonym": "Bahrainian",
		"continent": "as",
		"es": "Bahrein"
	},
	"BI": {
		"alpha3": "BDI",
		"code": "afbdi",
		"en": "Burundi",
		"pt": "Burundi",
		"fullname": "Burundi",
		"demonym": "Burundian",
		"continent": "af",
		"es": "Burundi"
	},
	"BJ": {
		"alpha3": "BEN",
		"code": "afben",
		"en": "Benin",
		"pt": "Benin",
		"fullname": "Benin",
		"demonym": "Beninese",
		"continent": "af",
		"es": "Benín"
	},
	"BT": {
		"alpha3": "BTN",
		"code": "asbtn",
		"en": "Bhutan",
		"pt": "Butão",
		"fullname": "Bhutan",
		"demonym": "Bhutanese",
		"continent": "as",
		"es": "Bhután"
	},
	"BW": {
		"alpha3": "BWA",
		"code": "afbwa",
		"en": "Botswana",
		"pt": "Botsuana",
		"fullname": "Botswana",
		"demonym": "Motswana",
		"continent": "af",
		"es": "Botsuana"
	},
	"BQ": {
		"alpha3": "BES",
		"fullname": "Bonaire",
		"code": "nabes",
		"demonym": "",
		"continent": "na"
	},
	"BR": {
		"alpha3": "BRA",
		"code": "sabra",
		"en": "Brazil",
		"pt": "Brasil",
		"fullname": "Brazil",
		"demonym": "Brazilian",
		"continent": "sa",
		"es": "Brasil"
	},
	"BS": {
		"alpha3": "BHS",
		"code": "nabhs",
		"en": "Bahamas",
		"pt": "Bahamas",
		"fullname": "Bahamas",
		"demonym": "Bahameese",
		"continent": "na",
		"es": "Bahamas"
	},
	"BY": {
		"alpha3": "BLR",
		"code": "eublr",
		"en": "Belarus",
		"pt": "Belarus",
		"fullname": "Belarus",
		"demonym": "Belarusian",
		"continent": "eu",
		"es": "Bielorrusia"
	},
	"BZ": {
		"alpha3": "BLZ",
		"code": "nablz",
		"en": "Belize",
		"pt": "Belize",
		"fullname": "Belize",
		"demonym": "Belizean",
		"continent": "na",
		"es": "Belice"
	},
	"RU": {
		"alpha3": "RUS",
		"code": "asrus",
		"en": "Russia",
		"pt": "Rússia",
		"fullname": "Russia",
		"demonym": "Russian",
		"continent": "eu",
		"es": "Rusia"
	},
	"RW": {
		"alpha3": "RWA",
		"code": "afrwa",
		"en": "Rwanda",
		"pt": "Ruanda",
		"fullname": "Rwanda",
		"demonym": "Rwandan",
		"continent": "af",
		"es": "Ruanda"
	},
	"RS": {
		"alpha3": "RSB",
		"code": "eursb",
		"en": "Serbia",
		"pt": "Serbia",
		"fullname": "Yugoslavia",
		"demonym": "Serbian",
		"continent": "eu",
		"es": "Serbia"
	},
	"RE": {
		"alpha3": "REU",
		"code": "afreu",
		"en": "Réunion",
		"pt": "Reunión",
		"fullname": "Reunion Island",
		"demonym": "",
		"continent": "af",
		"es": "Reunión"
	},
	"RO": {
		"alpha3": "ROU",
		"code": "eurou",
		"en": "Romania",
		"pt": "Romênia",
		"fullname": "Romania",
		"demonym": "Romanian",
		"continent": "eu",
		"es": "Rumanía"
	},
	"OM": {
		"alpha3": "OMN",
		"code": "asomn",
		"en": "Oman",
		"pt": "Omã",
		"fullname": "Oman",
		"demonym": "Omani",
		"continent": "as",
		"es": "Omán"
	},
	"HR": {
		"alpha3": "HRV",
		"code": "euhrv",
		"en": "Croatia",
		"pt": "Croácia",
		"fullname": "Croatia",
		"demonym": "Croatian",
		"continent": "eu",
		"es": "Croacia"
	},
	"HT": {
		"alpha3": "HTI",
		"code": "nahti",
		"en": "Haiti",
		"pt": "Haiti",
		"fullname": "Haiti",
		"demonym": "Haitian",
		"continent": "na",
		"es": "Haití"
	},
	"HU": {
		"alpha3": "HUN",
		"code": "euhun",
		"en": "Hungary",
		"pt": "Hungria",
		"fullname": "Hungary",
		"demonym": "Hungarian",
		"continent": "eu",
		"es": "Hungría"
	},
	"HK": {
		"alpha3": "HKG",
		"code": "ashkg",
		"en": "Hong Kong",
		"pt": "Hong Kong",
		"fullname": "Hong Kong",
		"demonym": "Hong Konger",
		"continent": "as",
		"es": "Hong kong"
	},
	"HN": {
		"alpha3": "HND",
		"code": "nahnd",
		"en": "Honduras",
		"pt": "Honduras",
		"fullname": "Honduras",
		"demonym": "Honduran",
		"continent": "na",
		"es": "Honduras"
	},
	"EE": {
		"alpha3": "EST",
		"code": "euest",
		"en": "Estonia",
		"pt": "Estônia",
		"fullname": "Estonia",
		"demonym": "Estonian",
		"continent": "eu",
		"es": "Estonia"
	},
	"EG": {
		"alpha3": "EGY",
		"code": "afegy",
		"en": "Egypt",
		"pt": "Egito",
		"fullname": "Egypt",
		"demonym": "Egyptian",
		"continent": "af",
		"es": "Egipto"
	},
	"EC": {
		"alpha3": "ECU",
		"code": "saecu",
		"en": "Ecuador",
		"pt": "Equador",
		"fullname": "Ecuador",
		"demonym": "Ecuadorean",
		"continent": "sa",
		"es": "Ecuador"
	},
	"ET": {
		"alpha3": "ETH",
		"code": "afeth",
		"en": "Ethiopia",
		"pt": "Etiópia",
		"fullname": "Ethiopia",
		"demonym": "Ethiopian",
		"continent": "af",
		"es": "Etiopía"
	},
	"ES": {
		"alpha3": "ESP",
		"code": "euesp",
		"en": "Spain",
		"pt": "Espanha",
		"fullname": "Spain",
		"demonym": "Spanish",
		"continent": "eu",
		"es": "España"
	},
	"ER": {
		"alpha3": "ERI",
		"code": "aferi",
		"en": "Eritrea",
		"pt": "Eritrea",
		"fullname": "Eritrea",
		"demonym": "Eritrean",
		"continent": "af",
		"es": "Eritrea"
	},
	"UY": {
		"alpha3": "URY",
		"code": "saury",
		"en": "Uruguay",
		"pt": "Uruguai",
		"fullname": "Uruguay",
		"demonym": "Uruguayan",
		"continent": "sa",
		"es": "Uruguay"
	},
	"UZ": {
		"alpha3": "UZB",
		"code": "asuzb",
		"en": "Uzbekistan",
		"pt": "Uzbekistán",
		"fullname": "Uzbekistan",
		"demonym": "Uzbekistani",
		"continent": "as",
		"es": "Uzbekistán"
	},
	"US": {
		"alpha3": "USA",
		"code": "nausa",
		"en": "United States of America",
		"pt": "Estados Unidos de América",
		"fullname": "United States",
		"demonym": "American",
		"continent": "na",
		"es": "Estados Unidos de América"
	},
	"UM": {
		"alpha3": "UMI",
		"code": "ocumi",
		"en": "United States Minor Outlying Islands",
		"pt": "Islas Ultramarinas Menores de Estados Unidos",
		"fullname": "Wake Island",
		"demonym": "United States Minor Outlying Islands",
		"continent": "oc",
		"es": "Islas Ultramarinas Menores de Estados Unidos"
	},
	"UG": {
		"alpha3": "UGA",
		"code": "afuga",
		"en": "Uganda",
		"pt": "Uganda",
		"fullname": "Uganda",
		"demonym": "Ugandan",
		"continent": "af",
		"es": "Uganda"
	},
	"UA": {
		"alpha3": "UKR",
		"code": "euukr",
		"en": "Ukraine",
		"pt": "Ucrânia",
		"fullname": "Ukraine",
		"demonym": "Ukrainian",
		"continent": "eu",
		"es": "Ucrania"
	},
	"VU": {
		"alpha3": "VUT",
		"code": "ocvut",
		"en": "Vanuatu",
		"pt": "Vanuatu",
		"fullname": "Vanuatu",
		"demonym": "Ni-Vanuatu",
		"continent": "oc",
		"es": "Vanuatu"
	},
	"NI": {
		"alpha3": "NIC",
		"code": "nanic",
		"en": "Nicaragua",
		"pt": "Nicarágua",
		"fullname": "Nicaragua",
		"demonym": "Nicaraguan",
		"continent": "na",
		"es": "Nicaragua"
	},
	"NL": {
		"alpha3": "NLD",
		"code": "eunld",
		"en": "Netherlands",
		"pt": "Países Baixos",
		"fullname": "Netherlands",
		"demonym": "Dutch",
		"continent": "eu",
		"es": "Países Bajos"
	},
	"NO": {
		"alpha3": "NOR",
		"code": "eunor",
		"en": "Norway",
		"pt": "Noruega",
		"fullname": "Norway",
		"demonym": "Norwegian",
		"continent": "eu",
		"es": "Noruega"
	},
	"NA": {
		"alpha3": "NAM",
		"code": "afnam",
		"en": "Namibia",
		"pt": "Namíbia",
		"fullname": "Namibia",
		"demonym": "Namibian",
		"continent": "af",
		"es": "Namibia"
	},
	"NC": {
		"alpha3": "NCL",
		"code": "ocncl",
		"en": "New Caledonia",
		"pt": "Nova Caledônia",
		"fullname": "New Caledonia",
		"demonym": "New Caledonian",
		"continent": "oc",
		"es": "Nueva Caledonia"
	},
	"NE": {
		"alpha3": "NER",
		"code": "afner",
		"en": "Niger",
		"pt": "Níger",
		"fullname": "Niger Republic",
		"demonym": "Nigerien",
		"continent": "af",
		"es": "Niger"
	},
	"NF": {
		"alpha3": "NFK",
		"code": "ocnfk",
		"en": "Norfolk Island",
		"pt": "Isla Norfolk",
		"fullname": "Norfolk Island",
		"demonym": "Norfolk Islander",
		"continent": "oc",
		"es": "Isla Norfolk"
	},
	"NG": {
		"alpha3": "NGA",
		"code": "afnga",
		"en": "Nigeria",
		"pt": "Nigéria",
		"fullname": "Nigeria",
		"demonym": "Nigerian",
		"continent": "af",
		"es": "Nigeria"
	},
	"NZ": {
		"alpha3": "NZL",
		"code": "ocnzl",
		"en": "New Zealand",
		"pt": "Nova Zelândia",
		"fullname": "New Zealand",
		"demonym": "New Zealander",
		"continent": "oc",
		"es": "Nueva Zelanda"
	},
	"NP": {
		"alpha3": "NPL",
		"code": "asnpl",
		"en": "Nepal",
		"pt": "Nepal",
		"fullname": "Nepal",
		"demonym": "Nepalese",
		"continent": "as",
		"es": "Nepal"
	},
	"NR": {
		"alpha3": "NRU",
		"code": "ocnru",
		"en": "Nauru",
		"pt": "Nauru",
		"fullname": "Nauru",
		"demonym": "Nauruan",
		"continent": "oc",
		"es": "Nauru"
	},
	"NU": {
		"alpha3": "NIU",
		"code": "ocniu",
		"en": "Niue",
		"pt": "Niue",
		"fullname": "Niue",
		"demonym": "Niuean",
		"continent": "oc",
		"es": "Niue"
	},
	"KG": {
		"alpha3": "KGZ",
		"code": "askgz",
		"en": "Kyrgyzstan",
		"pt": "Kirgizstán",
		"fullname": "Kyrgyzstan",
		"demonym": "Kyrgyzstani",
		"continent": "as",
		"es": "Kirgizstán"
	},
	"KE": {
		"alpha3": "KEN",
		"code": "afken",
		"en": "Kenya",
		"pt": "Quênia",
		"fullname": "Kenya",
		"demonym": "Kenyan",
		"continent": "af",
		"es": "Kenia"
	},
	"KI": {
		"alpha3": "KIR",
		"code": "ockir",
		"en": "Kiribati",
		"pt": "Kiribati",
		"fullname": "Kiribati",
		"demonym": "I-Kiribati",
		"continent": "oc",
		"es": "Kiribati"
	},
	"KH": {
		"alpha3": "KHM",
		"code": "askhm",
		"en": "Cambodia",
		"pt": "Camboja",
		"fullname": "Cambodia",
		"demonym": "Cambodian",
		"continent": "as",
		"es": "Camboya"
	},
	"KN": {
		"alpha3": "KNA",
		"code": "nakna",
		"en": "Saint Kitts and Nevis",
		"pt": "San Cristóbal y Nieves",
		"fullname": "St. Kitts",
		"demonym": "Kittian",
		"continent": "na",
		"es": "San Cristóbal y Nieves"
	},
	"KM": {
		"alpha3": "COM",
		"code": "afcom",
		"en": "Comoros",
		"pt": "Comoras",
		"fullname": "Comoros",
		"demonym": "Comoran",
		"continent": "af",
		"es": "Comoras"
	},
	"KR": {
		"alpha3": "KOR",
		"code": "askor",
		"en": "South Korea",
		"pt": "Coréia do Sul",
		"fullname": "South Korea",
		"demonym": "South Korean",
		"continent": "as",
		"es": "Corea del Sur"
	},
	"KP": {
		"alpha3": "PRK",
		"code": "asprk",
		"en": "North Korea",
		"pt": "Coréia do Norte",
		"fullname": "North Korea",
		"demonym": "North Korean",
		"continent": "as",
		"es": "Corea del Norte"
	},
	"KW": {
		"alpha3": "KWT",
		"code": "askwt",
		"en": "Kuwait",
		"pt": "Kuait",
		"fullname": "Kuwait",
		"demonym": "Kuwaiti",
		"continent": "as",
		"es": "Kuwait"
	},
	"KZ": {
		"alpha3": "KAZ",
		"code": "askaz",
		"en": "Kazakhstan",
		"pt": "Cazaquistão",
		"fullname": "Kazakhstan",
		"demonym": "Kazakhstani",
		"continent": "as",
		"es": "Kazajistán"
	},
	"KY": {
		"alpha3": "CYM",
		"code": "nacym",
		"en": "Cayman Islands",
		"pt": "Ilhas Cayman",
		"fullname": "Cayman Islands",
		"demonym": "Caymanian",
		"continent": "na",
		"es": "Islas Caimán"
	},
	"DO": {
		"alpha3": "DOM",
		"code": "nadom",
		"en": "Dominican Republic",
		"pt": "República Dominicana",
		"fullname": "Dominican Republic",
		"demonym": "Dominican",
		"continent": "na",
		"es": "República Dominicana"
	},
	"DM": {
		"alpha3": "DMA",
		"code": "nadma",
		"en": "Dominica",
		"pt": "Dominica",
		"fullname": "Dominica",
		"demonym": "Dominican",
		"continent": "na",
		"es": "Dominica"
	},
	"DJ": {
		"alpha3": "DJI",
		"code": "afdji",
		"en": "Djibouti",
		"pt": "Yibuti",
		"fullname": "Djibouti",
		"demonym": "Djiboutian",
		"continent": "af",
		"es": "Yibuti"
	},
	"DK": {
		"alpha3": "DNK",
		"code": "eudnk",
		"en": "Denmark",
		"pt": "Dinamarca",
		"fullname": "Denmark",
		"demonym": "Danish",
		"continent": "eu",
		"es": "Dinamarca"
	},
	"DE": {
		"alpha3": "DEU",
		"code": "eudeu",
		"en": "Germany",
		"pt": "Alemanha",
		"fullname": "Germany",
		"demonym": "German",
		"continent": "eu",
		"es": "Alemania"
	},
	"DZ": {
		"alpha3": "DZA",
		"code": "afdza",
		"en": "Algeria",
		"pt": "Algeria",
		"fullname": "Algeria",
		"demonym": "Algerian",
		"continent": "af",
		"es": "Algeria"
	},
	"TZ": {
		"alpha3": "TZA",
		"code": "aftza",
		"en": "Tanzania",
		"pt": "Tanzânia",
		"fullname": "Zanzibar",
		"demonym": "Tanzanian",
		"continent": "af",
		"es": "Tanzania"
	},
	"TV": {
		"alpha3": "TUV",
		"code": "octuv",
		"en": "Tuvalu",
		"pt": "Tuvalu",
		"fullname": "Tuvalu",
		"demonym": "Tuvaluan",
		"continent": "oc",
		"es": "Tuvalu"
	},
	"TW": {
		"alpha3": "TWN",
		"code": "astwn",
		"en": "Taiwan",
		"pt": "Taiwan",
		"fullname": "Taiwan",
		"demonym": "Taiwanese",
		"continent": "as",
		"es": "Taiwán"
	},
	"TT": {
		"alpha3": "TTO",
		"code": "natto",
		"en": "Trinidad and Tobago",
		"pt": "Trinidad e Tobago",
		"fullname": "Trinidad and Tobago",
		"demonym": "Trinidadian",
		"continent": "na",
		"es": "Trinidad y Tobago"
	},
	"TR": {
		"alpha3": "TUR",
		"code": "astur",
		"en": "Turkey",
		"pt": "Turquia",
		"fullname": "Turkey",
		"demonym": "Turkish",
		"continent": "as",
		"es": "Turquía"
	},
	"TN": {
		"alpha3": "TUN",
		"code": "aftun",
		"en": "Tunisia",
		"pt": "Tunísia",
		"fullname": "Tunisia",
		"demonym": "Tunisian",
		"continent": "af",
		"es": "Tunez"
	},
	"TO": {
		"alpha3": "TON",
		"code": "octon",
		"en": "Tonga",
		"pt": "Tonga",
		"fullname": "Tonga",
		"demonym": "Tongan",
		"continent": "oc",
		"es": "Tonga"
	},
	"TL": {
		"alpha3": "TLS",
		"code": "octls",
		"en": "East Timor",
		"pt": "Timor Oriental",
		"fullname": "East Timor",
		"demonym": "Timorese",
		"continent": "oc",
		"es": "Timor Oriental"
	},
	"TM": {
		"alpha3": "TKM",
		"code": "astkm",
		"en": "Turkmenistan",
		"pt": "Turkmenistán",
		"fullname": "Turkmenistan",
		"demonym": "Turkmen",
		"continent": "as",
		"es": "Turkmenistán"
	},
	"TJ": {
		"alpha3": "TJK",
		"code": "astjk",
		"en": "Tajikistan",
		"pt": "Tadjikistán",
		"fullname": "Tajikistan",
		"demonym": "Tajikistani",
		"continent": "as",
		"es": "Tadjikistán"
	},
	"TK": {
		"alpha3": "TKL",
		"code": "octkl",
		"en": "Tokelau",
		"pt": "Tokelau",
		"fullname": "Tokelau",
		"demonym": "Tokelauan",
		"continent": "oc",
		"es": "Tokelau"
	},
	"TH": {
		"alpha3": "THA",
		"code": "astha",
		"en": "Thailand",
		"pt": "Tailândia",
		"fullname": "Thailand",
		"demonym": "Thai",
		"continent": "as",
		"es": "Tailandia"
	},
	"TG": {
		"alpha3": "TGO",
		"code": "aftgo",
		"en": "Togo",
		"pt": "Togo",
		"fullname": "Togo",
		"demonym": "Togolese",
		"continent": "af",
		"es": "Togo"
	},
	"TD": {
		"alpha3": "TCD",
		"code": "aftcd",
		"en": "Chad",
		"pt": "Chade",
		"fullname": "Chad Republic",
		"demonym": "Chadian",
		"continent": "af",
		"es": "Chad"
	},
	"TC": {
		"alpha3": "TCA",
		"code": "natca",
		"en": "Turks and Caicos Islands",
		"pt": "Islas Turcas y Caicos",
		"fullname": "Turks and Caicos Islands",
		"demonym": "Turks and Caicos Islander",
		"continent": "na",
		"es": "Islas Turcas y Caicos"
	},
	"AE": {
		"alpha3": "ARE",
		"code": "asare",
		"en": "United Arab Emirates",
		"pt": "Emirados Árabes Unidos",
		"fullname": "United Arab Emirates",
		"demonym": "Emirian",
		"continent": "as",
		"es": "Emiratos Árabes Unidos"
	},
	"AD": {
		"alpha3": "AND",
		"code": "euand",
		"en": "Andorra",
		"pt": "Andorra",
		"fullname": "Andorra",
		"demonym": "Andorran",
		"continent": "eu",
		"es": "Andorra"
	},
	"AG": {
		"alpha3": "ATG",
		"code": "naatg",
		"en": "Antigua and Barbuda",
		"pt": "Antigua y Barbuda",
		"fullname": "Antigua and Barbuda",
		"demonym": "Antiguan",
		"continent": "na",
		"es": "Antigua y Barbuda"
	},
	"AF": {
		"alpha3": "AFG",
		"code": "asafg",
		"en": "Afghanistan",
		"pt": "Afeganistão",
		"fullname": "Afghanistan",
		"demonym": "Afghani",
		"continent": "as",
		"es": "Afganistán"
	},
	"AI": {
		"alpha3": "AIA",
		"code": "naaia",
		"en": "Anguilla",
		"pt": "Anguila",
		"fullname": "Anguilla",
		"demonym": "Anguillan",
		"continent": "na",
		"es": "Anguila"
	},
	"AM": {
		"alpha3": "ARM",
		"code": "asarm",
		"en": "Armenia",
		"pt": "Armênia",
		"fullname": "Armenia",
		"demonym": "Armenian",
		"continent": "as",
		"es": "Armenia"
	},
	"AL": {
		"alpha3": "ALB",
		"code": "eualb",
		"en": "Albania",
		"pt": "Albânia",
		"fullname": "Albania",
		"demonym": "Albanian",
		"continent": "eu",
		"es": "Albania"
	},
	"AO": {
		"alpha3": "AGO",
		"code": "afago",
		"en": "Angola",
		"pt": "Angola",
		"fullname": "Angola",
		"demonym": "Angolan",
		"continent": "af",
		"es": "Angola"
	},
	"AQ": {
		"alpha3": "ATA",
		"code": "anata",
		"en": "Antarctica",
		"pt": "Antártida",
		"fullname": "Antarctica",
		"demonym": "Antarctic",
		"continent": "an",
		"es": "Antártida"
	},
	"AS": {
		"alpha3": "ASM",
		"code": "ocasm",
		"en": "American Samoa",
		"pt": "Samoa Americana",
		"fullname": "American Samoa",
		"demonym": "Samoan",
		"continent": "oc",
		"es": "Samoa Americana"
	},
	"AR": {
		"alpha3": "ARG",
		"code": "saarg",
		"en": "Argentina",
		"pt": "Argentina",
		"fullname": "Argentina",
		"demonym": "Argentine",
		"continent": "sa",
		"es": "Argentina"
	},
	"AU": {
		"alpha3": "AUS",
		"code": "ocaus",
		"en": "Australia",
		"pt": "Austrália",
		"fullname": "Australia",
		"demonym": "Australian",
		"continent": "oc",
		"es": "Australia"
	},
	"AT": {
		"alpha3": "AUT",
		"code": "euaut",
		"en": "Austria",
		"pt": "Áustria",
		"fullname": "Austria",
		"demonym": "Austrian",
		"continent": "eu",
		"es": "Austria"
	},
	"AW": {
		"alpha3": "ABW",
		"code": "naabw",
		"en": "Aruba",
		"pt": "Aruba",
		"fullname": "Aruba",
		"demonym": "Arubian",
		"continent": "na",
		"es": "Aruba"
	},
	"AZ": {
		"alpha3": "AZE",
		"code": "asaze",
		"en": "Azerbaijan",
		"pt": "Azerbaijão",
		"fullname": "Azerbaijan",
		"demonym": "Azerbaijani",
		"continent": "as",
		"es": "Azerbayán"
	},
	"QA": {
		"alpha3": "QAT",
		"code": "asqat",
		"en": "Qatar",
		"pt": "Catar",
		"fullname": "Qatar",
		"demonym": "Qatari",
		"continent": "as",
		"es": "Qatar"
	}
}
