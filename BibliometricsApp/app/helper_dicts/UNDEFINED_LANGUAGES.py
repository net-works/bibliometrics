UNDEFINED_LANGUAGES = {
	'in':  {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	},
	'zz': {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	},
	'sp': {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	},
	'nd': {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	},
	'al': {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	},
	'gr': {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	},
	'up': {
		'alpha3': 'glv',
		'en': 'Undefined',
		'es': 'Indefinido',
		'pt': 'Indefinido',
		'english': 'Undefined'
	}
}