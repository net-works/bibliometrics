#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA2_DEMONYM = {	
	"AD": {
		"fullname": "Andorra",
		"demonym": "Andorran"
	},
	"AE": {
		"fullname": "United Arab Emirates",
		"demonym": "Emirian"
	},
	"AF": {
		"fullname": "Afghanistan",
		"demonym": "Afghani",
	},
	"AG": {
		"fullname": "Antigua and Barbuda",
		"demonym": "Antiguan"
	},
	"AI": {
		"fullname": "Anguilla",
		"demonym": "Anguillan"
	},
	"AL": {
		"fullname": "Albania",
		"demonym": "Albanian"
	},
	"AM": {
		"fullname": "Armenia",
		"demonym": "Armenian"
	},
	"AO": {
		"fullname": "Angola",
		"demonym": "Angolan"
	},
	"AQ": {
		"fullname": "Antarctica",
		"demonym": "Antarctic"
	},
	"AR": {
		"fullname": "Argentina",
		"demonym": "Argentine"
	},
	"AS": {
		"fullname": "American Samoa",
		"demonym": "Samoan"
	},
	"AT": {
		"fullname": "Austria",
		"demonym": "Austrian"
	},
	"AU": {
		"fullname": "Australia",
		"demonym": "Australian"
	},
	"AW": {
		"fullname": "Aruba",
		"demonym": "Arubian"
	},
	"AX": {
		"fullname": "Aland Islands",
		"demonym": "Alandic"
	},
	"AZ": {
		"fullname": "Azerbaijan",
		"demonym": "Azerbaijani"
	},
	"BA": {
		"fullname": "Bosnia and Herzegovina",
		"demonym": "Bosnian"
	},
	"BB": {
		"fullname": "Barbados",
		"demonym": "Barbadian"
	},
	"BD": {
		"fullname": "Bangladesh",
		"demonym": "Bangladeshi"
	},
	"BE": {
		"fullname": "Belgium",
		"demonym": "Belgian"
	},
	"BF": {
		"fullname": "Burkina Faso",
		"demonym": "Burkinabe"
	},
	"BG": {
		"fullname": "Bulgaria",
		"demonym": "Bulgarian"
	},
	"BH": {
		"fullname": "Bahrain",
		"demonym": "Bahrainian"
	},
	"BI": {
		"fullname": "Burundi",
		"demonym": "Burundian"
	},
	"BJ": {
		"fullname": "Benin",
		"demonym": "Beninese"
	},
	"BL": {
		"fullname": "Saint Barthélemy",
		"demonym": "Barthélemois"
	},
	"BM": {
		"fullname": "Bermuda",
		"demonym": "Bermudan"
	},
	"BN": {
		"fullname": "Brunei",
		"demonym": "Bruneian"
	},
	"BO": {
		"fullname": "Bolivia",
		"demonym": "Bolivian"
	},
	"BQ": {
		"fullname": "Caribbean Netherlands",
		"demonym": ""
	},
	"BR": {
		"fullname": "Brazil",
		"demonym": "Brazilian"
	},
	"BS": {
		"fullname": "Bahamas",
		"demonym": "Bahameese"
	},
	"BT": {
		"fullname": "Bhutan",
		"demonym": "Bhutanese"
	},
	"BV": {
		"fullname": "Bouvet Island",
		"demonym": ""
	},
	"BW": {
		"fullname": "Botswana",
		"demonym": "Motswana"
	},
	"BY": {
		"fullname": "Belarus",
		"demonym": "Belarusian"
	},
	"BZ": {
		"fullname": "Belize",
		"demonym": "Belizean"
	},
	"CA": {
		"fullname": "Canada",
		"demonym": "Canadian"
	},
	"CC": {
		"fullname": "Cocos (Keeling) Islands",
		"demonym": "Cocossian"
	},
	"CD": {
		"fullname": "Democratic Republic of the Congo",
		"demonym": "Congolese"
	},
	"CF": {
		"fullname": "Central African Republic",
		"demonym": "Central African"
	},
	"CG": {
		"fullname": "Congo (Republic of)",
		"demonym": "Congolese"
	},
	"CH": {
		"fullname": "Switzerland",
		"demonym": "Swiss"
	},
	"CI": {
		"fullname": "Ivory Coast",
		"demonym": "Ivorian"
	},
	"CK": {
		"fullname": "Cook Islands",
		"demonym": "Cook Islander"
	},
	"CL": {
		"fullname": "Chile",
		"demonym": "Chilean"
	},
	"CM": {
		"fullname": "Cameroon",
		"demonym": "Cameroonian"
	},
	"CN": {
		"fullname": "China",
		"demonym": "Chinese"
	},
	"CO": {
		"fullname": "Colombia",
		"demonym": "Colombian"
	},
	"CR": {
		"fullname": "Costa Rica",
		"demonym": "Costa Rican"
	},
	"CU": {
		"fullname": "Cuba",
		"demonym": "Cuban"
	},
	"CV": {
		"fullname": "Cape Verde",
		"demonym": "Cape Verdean"
	},
	"CW": {
		"fullname": "Curaçao",
		"demonym": "Curaçaoan"
	},
	"CX": {
		"fullname": "Christmas Island",
		"demonym": "Christmas Islander"
	},
	"CY": {
		"fullname": "Cyprus",
		"demonym": "Cypriot"
	},
	"CZ": {
		"fullname": "Czech Republic",
		"demonym": "Czech"
	},
	"DE": {
		"fullname": "Germany",
		"demonym": "German"
	},
	"DJ": {
		"fullname": "Djibouti",
		"demonym": "Djiboutian"
	},
	"DK": {
		"fullname": "Denmark",
		"demonym": "Danish",
	},
	"DM": {
		"fullname": "Dominica",
		"demonym": "Dominican"
	},
	"DO": {
		"fullname": "Dominican Republic",
		"demonym": "Dominican"
	},
	"DZ": {
		"fullname": "Algeria",
		"demonym": "Algerian"
	},
	"EC": {
		"fullname": "Ecuador",
		"demonym": "Ecuadorean"
	},
	"EE": {
		"fullname": "Estonia",
		"demonym": "Estonian"
	},
	"EG": {
		"fullname": "Egypt",
		"demonym": "Egyptian"
	},
	"EH": {
		"fullname": "Western Saharan",
		"demonym": "Western Saharan"
	},
	"ER": {
		"fullname": "Eritrea",
		"demonym": "Eritrean"
	},
	"ES": {
		"fullname": "Spain",
		"demonym": "Spanish"
	},
	"ET": {
		"fullname": "Ethiopia",
		"demonym": "Ethiopian"
	},
	"FI": {
		"fullname": "Finland",
		"demonym": "Finnish"
	},
	"FJ": {
		"fullname": "Fiji",
		"demonym": "Fijian"
	},
	"FK": {
		"fullname": "Falkland Islands",
		"demonym": "Falkland Islander"
	},
	"FM": {
		"fullname": "Micronesia",
		"demonym": "Micronesian"
	},
	"FO": {
		"fullname": "Faroe Islands",
		"demonym": "Faroese"
	},
	"FR": {
		"fullname": "France",
		"demonym": "French"
	},
	"GA": {
		"fullname": "Gabon",
		"demonym": "Gabonese"
	},
	"GB": {
		"fullname": "United Kingdom",
		"demonym": "British"
	},
	"GD": {
		"fullname": "Grenada",
		"demonym": "Grenadian"
	},
	"GE": {
		"fullname": "Georgia",
		"demonym": "Georgian"
	},
	"GF": {
		"fullname": "French Guiana",
		"demonym": "French Guianese"
	},
	"GG": {
		"fullname": "Guernsey",
		"demonym": "Guernsey"
	},
	"GH": {
		"fullname": "Ghana",
		"demonym": "Ghanaian"
	},
	"GI": {
		"fullname": "Gibralter",
		"demonym": "Gibralterian"
	},
	"GL": {
		"fullname": "Greenland",
		"demonym": "Greenlander"
	},
	"GM": {
		"fullname": "Gambia",
		"demonym": "Gambian"
	},
	"GN": {
		"fullname": "Guinea",
		"demonym": "Guinean"
	},
	"GP": {
		"fullname": "Guadeloupe",
		"demonym": "Guadeloupean"
	},
	"GQ": {
		"fullname": "Equatorial Guinea",
		"demonym": "Equatorial Guinean"
	},
	"GR": {
		"fullname": "Greece",
		"demonym": "Greek"
	},
	"GS": {
		"fullname": "South Georgia and the South Sandwich Islands",
		"demonym": "South Georgia and the South Sandwich Islands"
	},
	"GT": {
		"fullname": "Guatemala",
		"demonym": "Guatemalan"
	},
	"GU": {
		"fullname": "Guam",
		"demonym": "Guamanian"
	},
	"GW": {
		"fullname": "Guinea-Bissau",
		"demonym": "Guinean"
	},
	"GY": {
		"fullname": "Guyana",
		"demonym": "Guyanese"
	},
	"HK": {
		"fullname": "Hong Kong",
		"demonym": "Hong Konger"
	},
	"HM": {
		"fullname": "Heard and McDonald Islands",
		"demonym": ""
	},
	"HN": {
		"fullname": "Honduras",
		"demonym": "Honduran"
	},
	"HR": {
		"fullname": "Croatia",
		"demonym": "Croatian"
	},
	"HT": {
		"fullname": "Haiti",
		"demonym": "Haitian"
	},
	"HU": {
		"fullname": "Hungary",
		"demonym": "Hungarian"
	},
	"ID": {
		"fullname": "Indonesia",
		"demonym": "Indonesian"
	},
	"IE": {
		"fullname": "Ireland",
		"demonym": "Irish"
	},
	"IL": {
		"fullname": "Israel",
		"demonym": "Israeli"
	},
	"IM": {
		"fullname": "Isle of Man",
		"demonym": "Manx"
	},
	"IN": {
		"fullname": "India",
		"demonym": "Indian"
	},
	"IO": {
		"fullname": "British Indian Ocean Territory",
		"demonym": "British Indian Ocean Territory"
	},
	"IQ": {
		"fullname": "Iraq",
		"demonym": "Iraqi"
	},
	"IR": {
		"fullname": "Iran",
		"demonym": "Iranian"
	},
	"IS": {
		"fullname": "Iceland",
		"demonym": "Iceland"
	},
	"IT": {
		"fullname": "Italy",
		"demonym": "Italian"
	},
	"JE": {
		"fullname": "Jersey",
		"demonym": "Jersey"
	},
	"JM": {
		"fullname": "Jamaica",
		"demonym": "Jamaican"
	},
	"JO": {
		"fullname": "Jordan",
		"demonym": "Jordanian"
	},
	"JP": {
		"fullname": "Japan",
		"demonym": "Japanese"
	},
	"KE": {
		"fullname": "Kenya",
		"demonym": "Kenyan"
	},
	"KG": {
		"fullname": "Kyrgyzstan",
		"demonym": "Kyrgyzstani"
	},
	"KH": {
		"fullname": "Cambodia",
		"demonym": "Cambodian"
	},
	"KI": {
		"fullname": "Kiribati",
		"demonym": "I-Kiribati"
	},
	"KM": {
		"fullname": "Comoros",
		"demonym": "Comoran"
	},
	"KN": {
		"fullname": "Saint Kitts and Nevis",
		"demonym": "Kittian"
	},
	"KP": {
		"fullname": "North Korea",
		"demonym": "North Korean"
	},
	"KR": {
		"fullname": "South Korea",
		"demonym": "South Korean"
	},
	"KW": {
		"fullname": "Kuwait",
		"demonym": "Kuwaiti"
	},
	"KY": {
		"fullname": "Cayman Islands",
		"demonym": "Caymanian"
	},
	"KZ": {
		"fullname": "Kazakhstan",
		"demonym": "Kazakhstani"
	},
	"LA": {
		"fullname": "Laos",
		"demonym": "Laotian"
	},
	"LB": {
		"fullname": "Lebanon",
		"demonym": "Lebanese"
	},
	"LC": {
		"fullname": "Saint Lucia",
		"demonym": "Saint Lucian"
	},
	"LI": {
		"fullname": "Liechtenstein",
		"demonym": "Liechtensteiner"
	},
	"LK": {
		"fullname": "Sri Lanka",
		"demonym": "Sri Lankan"
	},
	"LR": {
		"fullname": "Liberia",
		"demonym": "Liberian"
	},
	"LS": {
		"fullname": "Lesotho",
		"demonym": "Mosotho"
	},
	"LT": {
		"fullname": "Lithuania",
		"demonym": "Lithunian"
	},
	"LU": {
		"fullname": "Luxembourg",
		"demonym": "Luxembourger"
	},
	"LV": {
		"fullname": "Latvia",
		"demonym": "Latvian"
	},
	"LY": {
		"fullname": "Libya",
		"demonym": "Libyan"
	},
	"MA": {
		"fullname": "Morocco",
		"demonym": "Moroccan"
	},
	"MC": {
		"fullname": "Monaco",
		"demonym": "Monacan"
	},
	"MD": {
		"fullname": "Moldova",
		"demonym": "Moldovan"
	},
	"ME": {
		"fullname": "Montenegro",
		"demonym": "Montenegrin"
	},
	"MF": {
		"fullname": "Saint Martin (France)",
		"demonym": ""
	},
	"MG": {
		"fullname": "Madagascar",
		"demonym": "Malagasy"
	},
	"MH": {
		"fullname": "Marshall Islands",
		"demonym": "Marshallese"
	},
	"MK": {
		"fullname": "Macedonia",
		"demonym": "Macedonian"
	},
	"ML": {
		"fullname": "Mali",
		"demonym": "Malian"
	},
	"MM": {
		"fullname": "Burma (Republic of the Union of Myanmar)",
		"demonym": "Myanmarese"
	},
	"MN": {
		"fullname": "Mongolia",
		"demonym": "Mongolian"
	},
	"MO": {
		"fullname": "Macau",
		"demonym": "Macanese"
	},
	"MP": {
		"fullname": "Northern Mariana Islands",
		"demonym": "Northern Mariana Islander"
	},
	"MQ": {
		"fullname": "Martinique",
		"demonym": "Martinican"
	},
	"MR": {
		"fullname": "Mauritania",
		"demonym": "Mauritanian"
	},
	"MS": {
		"fullname": "Montserrat",
		"demonym": "Montserratian"
	},
	"MT": {
		"fullname": "Malta",
		"demonym": "Maltese"
	},
	"MU": {
		"fullname": "Mauritius",
		"demonym": "Mauritian"
	},
	"MV": {
		"fullname": "Maldives",
		"demonym": "Maldivan"
	},
	"MW": {
		"fullname": "Malawi",
		"demonym": "Malawian"
	},
	"MX": {
		"fullname": "Mexico",
		"demonym": "Mexican"
	},
	"MY": {
		"fullname": "Malaysia",
		"demonym": "Malaysian"
	},
	"MZ": {
		"fullname": "Mozambique",
		"demonym": "Mozambican"
	},
	"NA": {
		"fullname": "Namibia",
		"demonym": "Namibian"
	},
	"NC": {
		"fullname": "New Caledonia",
		"demonym": "New Caledonian"
	},
	"NE": {
		"fullname": "Niger",
		"demonym": "Nigerien"
	},
	"NF": {
		"fullname": "Norfolk Island",
		"demonym": "Norfolk Islander"
	},
	"NG": {
		"fullname": "Nigeria",
		"demonym": "Nigerian"
	},
	"NI": {
		"fullname": "Nicaragua",
		"demonym": "Nicaraguan"
	},
	"NL": {
		"fullname": "Netherlands",
		"demonym": "Dutch"
	},
	"NO": {
		"fullname": "Norway",
		"demonym": "Norwegian"
	},
	"NP": {
		"fullname": "Nepal",
		"demonym": "Nepalese"
	},
	"NR": {
		"fullname": "Nauru",
		"demonym": "Nauruan"
	},
	"NU": {
		"fullname": "Niue",
		"demonym": "Niuean"
	},
	"NZ": {
		"fullname": "New Zealand",
		"demonym": "New Zealander"
	},
	"OM": {
		"fullname": "Oman",
		"demonym": "Omani"
	},
	"PA": {
		"fullname": "Panama",
		"demonym": "Panamanian"
	},
	"PE": {
		"fullname": "Peru",
		"demonym": "Peruvian"
	},
	"PF": {
		"fullname": "French Polynesia",
		"demonym": "French Polynesian"
	},
	"PG": {
		"fullname": "Papua New Guinea",
		"demonym": "Papua New Guinean"
	},
	"PH": {
		"fullname": "Philippines",
		"demonym": "Filipino"
	},
	"PK": {
		"fullname": "Pakistan",
		"demonym": "Pakistani"
	},
	"PL": {
		"fullname": "Poland",
		"demonym": "Polish"
	},
	"PM": {
		"fullname": "St. Pierre and Miquelon",
		"demonym": "Saint-Pierrais"
	},
	"PN": {
		"fullname": "Pitcairn",
		"demonym": "Pitcairn Islander"
	},
	"PR": {
		"fullname": "Puerto Rico",
		"demonym": "Puerto Rican"
	},
	"PS": {
		"fullname": "Palestine",
		"demonym": "Palestinian"
	},
	"PT": {
		"fullname": "Portugal",
		"demonym": "Portuguese"
	},
	"PW": {
		"fullname": "Palau",
		"demonym": "Palauan"
	},
	"PY": {
		"fullname": "Paraguay",
		"demonym": "Paraguayan"
	},
	"QA": {
		"fullname": "Qatar",
		"demonym": "Qatari"
	},
	"RE": {
		"fullname": "Réunion",
		"demonym": ""
	},
	"RO": {
		"fullname": "Romania",
		"demonym": "Romanian"
	},
	"RS": {
		"fullname": "Serbia",
		"demonym": "Serbian"
	},
	"RU": {
		"fullname": "Russian Federation",
		"demonym": "Russian"
	},
	"RW": {
		"fullname": "Rwanda",
		"demonym": "Rwandan"
	},
	"SA": {
		"fullname": "Saudi Arabia",
		"demonym": "Saudi Arabian"
	},
	"SB": {
		"fullname": "Solomon Islands",
		"demonym": "Solomon Islander"
	},
	"SC": {
		"fullname": "Seychelles",
		"demonym": "Seychellois"
	},
	"SD": {
		"fullname": "Sudan",
		"demonym": "Sudanese"
	},
	"SE": {
		"fullname": "Sweden",
		"demonym": "Swedish"
	},
	"SG": {
		"fullname": "Singapore",
		"demonym": "Singaporean"
	},
	"SH": {
		"fullname": "Saint Helena",
		"demonym": "Saint Helenian"
	},
	"SI": {
		"fullname": "Slovenia",
		"demonym": "Slovenian"
	},
	"SJ": {
		"fullname": "Svalbard and Jan Mayen Islands",
		"demonym": ""
	},
	"SK": {
		"fullname": "Slovakia",
		"demonym": "Slovakian"
	},
	"SL": {
		"fullname": "Sierra Leone",
		"demonym": "Sierra Leonean"
	},
	"SM": {
		"fullname": "San Marino",
		"demonym": "Sanmarinese"
	},
	"SN": {
		"fullname": "Senegal",
		"demonym": "Senegalese"
	},
	"SO": {
		"fullname": "Somalia",
		"demonym": "Somali"
	},
	"SR": {
		"fullname": "Suriname",
		"demonym": "Surinamer"
	},
	"SS": {
		"fullname": "South Sudan",
		"demonym": "Sudanese"
	},
	"ST": {
		"fullname": "São Tome and Príncipe",
		"demonym": "São Tomean"
	},
	"SV": {
		"fullname": "El Salvador",
		"demonym": "Salvadorean"
	},
	"SX": {
		"fullname": "Saint Martin (Netherlands)",
		"demonym": "Saint Martin (Netherlands)"
	},
	"SY": {
		"fullname": "Syria",
		"demonym": "Syrian"
	},
	"SZ": {
		"fullname": "Swaziland",
		"demonym": "Swazi"
	},
	"TC": {
		"fullname": "Turks and Caicos Islands",
		"demonym": "Turks and Caicos Islander"
	},
	"TD": {
		"fullname": "Chad",
		"demonym": "Chadian"
	},
	"TF": {
		"fullname": "French Southern Territories",
		"demonym": "French Southern Territories"
	},
	"TG": {
		"fullname": "Togo",
		"demonym": "Togolese"
	},
	"TH": {
		"fullname": "Thailand",
		"demonym": "Thai"
	},
	"TJ": {
		"fullname": "Tajikistan",
		"demonym": "Tajikistani"
	},
	"TK": {
		"fullname": "Tokelau",
		"demonym": "Tokelauan"
	},
	"TL": {
		"fullname": "Timor-Leste",
		"demonym": "Timorese"
	},
	"TM": {
		"fullname": "Turkmenistan",
		"demonym": "Turkmen"
	},
	"TN": {
		"fullname": "Tunisia",
		"demonym": "Tunisian"
	},
	"TO": {
		"fullname": "Tonga",
		"demonym": "Tongan"
	},
	"TR": {
		"fullname": "Turkey",
		"demonym": "Turkish"
	},
	"TT": {
		"fullname": "Trinidad and Tobago",
		"demonym": "Trinidadian"
	},
	"TV": {
		"fullname": "Tuvalu",
		"demonym": "Tuvaluan"
	},
	"TW": {
		"fullname": "Taiwan",
		"demonym": "Taiwanese"
	},
	"TZ": {
		"fullname": "Tanzania",
		"demonym": "Tanzanian"
	},
	"UA": {
		"fullname": "Ukraine",
		"demonym": "Ukrainian"
	},
	"UG": {
		"fullname": "Uganda",
		"demonym": "Ugandan"
	},
	"UM": {
		"fullname": "United States Minor Outlying Islands",
		"demonym": "United States Minor Outlying Islands"
	},
	"US": {
		"fullname": "United States of America",
		"demonym": "American"
	},
	"UY": {
		"fullname": "Uruguay",
		"demonym": "Uruguayan"
	},
	"UZ": {
		"fullname": "Uzbekistan",
		"demonym": "Uzbekistani"
	},
	"VA": {
		"fullname": "Vatican",
		"demonym": "Vatican"
	},
	"VC": {
		"fullname": "Saint Vincent and Grenadines",
		"demonym": "Saint Vincentian"
	},
	"VE": {
		"fullname": "Venezuela",
		"demonym": "Venezuelan"
	},
	"VG": {
		"fullname": "British Virgin Islands",
		"demonym": "Virgin Islander"
	},
	"VI": {
		"fullname": "United States Virgin Islands",
		"demonym": "Virgin Islander"
	},
	"VN": {
		"fullname": "Vietnam",
		"demonym": "Vietnamese"
	},
	"VU": {
		"fullname": "Vanuatu",
		"demonym": "Ni-Vanuatu"
	},
	"WF": {
		"fullname": "Wallis and Futuna Islands",
		"demonym": "Wallisian"
	},
	"WS": {
		"fullname": "Samoa",
		"demonym": "Samoan"
	},
	"YE": {
		"fullname": "Yemen",
		"demonym": "Yemeni"
	},
	"YT": {
		"fullname": "Mayotte",
		"demonym": "Mahoran"
	},
	"ZA": {
		"fullname": "South Africa",
		"demonym": "South African"
	},
	"ZM": {
		"fullname": "Zambia",
		"demonym": "Zambian"
	},
	"ZW": {
		"fullname": "Zimbabwe",
		"demonym": "Zimbabwean"
	}
}
