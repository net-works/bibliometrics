#!/usr/bin/env python
# -*- coding: utf-8 -*-
DOCUMENT_TYPE = {
  "research-article": {
    "en": "Article",
    "es": "Artículo",
    "pt": "Artigo",
  },
  "editorial": {
    "en": "Editorial",
    "es": "Editorial",
    "pt": "Editorial",
  },
  "book-review": {
    "en": "Book review",
    "es": "Reseña de Libro",
    "pt": "Resenha de livro",
  },
  "case-report": {
    "en": "Case report",
    "es": "Informe de caso",
    "pt": "Relato de caso",
  },
  "review-article": {
    "en": "Review article",
    "es": "Artículo de revisión",
    "pt": "Artigo de revisão",
  },
  "rapid-communication": {
    "en": "Rapid communication",
    "es": "Comunicación rápida",
    "pt": "Comunicação rápida",
  },
  "letter": {
    "en": "Letter",
    "es": "Carta",
    "pt": "Carta",
  },
  "brief-report": {
    "en": "Brief report",
    "es": "Informe breve",
    "pt": "Relato breve",
  },
  "article-commentary": {
    "en": "Article-commentary",
    "es": "Artículo-comentario",
    "pt": "Artigo-comentário",
  },
  "abstract": {
    "en": "Abstract",
    "es": "Resumen",
    "pt": "Resumo",
  },
  "other": {
    "en": "Other",
    "es": "Otros",
    "pt": "Outros",
  },
  "addendum": {
    "en": "Addendum",
    "es": "Addendum",
    "pt": "Addendum",
  },
  "press-release": {
    "en": "Press release",
    "es": "Comunicado de prensa",
    "pt": "Comunicado de imprensa",
  },
  "news": {
    "en": "News",
    "es": "Noticia",
    "pt": "Notícia",
  },
  "correction": {
    "en": "Correction",
    "es": "Corrección",
    "pt": "Correção",
  }
}